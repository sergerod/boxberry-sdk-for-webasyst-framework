<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ListZipsCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ListZipsCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ListZipsItem[] */
    protected $_items = [];

    /**
     * ListZipsCollection constructor.
     * @param ListZipsItem[] $items
     */
    public function __construct(ListZipsItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ListZipsCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_map(function (array $i) {
            return ListZipsItem::fromArray($i);
        }, $values));
    }
}