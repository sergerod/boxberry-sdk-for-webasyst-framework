<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class PointsForParcelsCollection
 * @package SergeR\BoxberrySDK\Type
 */
class PointsForParcelsCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var PointsForParcelsItem[] */
    protected $_items = [];

    /**
     * PointsForParcelsCollection constructor.
     * @param PointsForParcelsItem[] $items
     */
    public function __construct(PointsForParcelsItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return PointsForParcelsCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($item) {
            if ($item instanceof PointsForParcelsItem) {
                return clone $item;
            } elseif (is_array($item)) {
                return PointsForParcelsItem::fromArray($item);
            }
            return null;
        }, $values)));
    }
}