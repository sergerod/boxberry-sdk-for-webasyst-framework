<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

/**
 * Class ParcelWeight
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelWeight
{

    protected $weight = 1;

    /** @var string|null */
    protected $barcode;

    /** @var int|null */
    protected $x;

    /** @var int|null */
    protected $y;

    /** @var int|null */
    protected $z;

    /**
     * @param int $weight
     * @return ParcelWeight
     */
    public function setWeight(int $weight): ParcelWeight
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param string|null $barcode
     * @return ParcelWeight
     */
    public function setBarcode(?string $barcode): ParcelWeight
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    /**
     * @param int|null $x
     * @return ParcelWeight
     */
    public function setX(?int $x): ParcelWeight
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getX(): ?int
    {
        return $this->x;
    }

    /**
     * @param int|null $y
     * @return ParcelWeight
     */
    public function setY(?int $y): ParcelWeight
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getY(): ?int
    {
        return $this->y;
    }

    /**
     * @param int|null $z
     * @return ParcelWeight
     */
    public function setZ(?int $z): ParcelWeight
    {
        $this->z = $z;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getZ(): ?int
    {
        return $this->z;
    }
}
