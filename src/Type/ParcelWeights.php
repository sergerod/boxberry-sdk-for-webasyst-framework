<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

/**
 * Class ParcelWeights
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelWeights implements \JsonSerializable, \Countable, \Iterator
{
    protected $weights = [];

    protected $position = 0;

    /**
     * ParcelWeights constructor.
     * @param ParcelWeight ...$weights
     */
    public function __construct(ParcelWeight ...$weights)
    {
        $this->weights = $weights;
    }

    /**
     * @return ParcelWeight[]
     */
    public function getWeights(): array
    {
        return $this->weights;
    }

    /**
     * @param ParcelWeight ...$weights
     * @return ParcelWeights
     */
    public function setWeights(ParcelWeight ...$weights): ParcelWeights
    {
        $this->weights = $weights;
        return $this;
    }

    /**
     * @param ParcelWeight $weight
     * @return $this
     */
    public function addWeight(ParcelWeight $weight): ParcelWeights
    {
        $this->weights[] = $weight;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $data = [];

        foreach ($this->weights as $key => $weight) {
            if ($key === 0) {
                $data['weight'] = (string)$weight->getWeight();
                if ($barcode = $weight->getBarcode()) $data['barcode'] = $barcode;
                if (($x = $weight->getX()) && ($y = $weight->getY()) && ($z = $weight->getZ()))
                    $data += ['x' => $x, 'y' => $y, 'z' => $z];
            } else {
                $cnt = (string)($key + 1);
                $data['weight' . $cnt] = (string)$weight->getWeight();
                if ($barcode = $weight->getBarcode()) $data['barcode' . $cnt] = $barcode;
            }
        }

        return $data;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->weights);
    }

    /**
     * Iterator implementation
     * @return ParcelWeight
     */
    public function current(): ParcelWeight
    {
        return $this->weights[$this->position];
    }

    /**
     * Iterator implementation
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Iterator implementation
     * @return int
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * Iterator implementation
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->weights[$this->position]);
    }

    /**
     * Iterator implementation
     */
    public function rewind()
    {
        $this->position = 0;
    }
}
