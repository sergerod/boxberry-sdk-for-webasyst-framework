<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ParcelItemCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelItemCollection implements \IteratorAggregate, FillableFromArray, \JsonSerializable
{
    use ArrayIteratorAggregateCollection;

    /** @var ParcelItem[] */
    protected $_items = [];

    /**
     * ParcelItemCollection constructor.
     * @param ParcelItem[] $items
     */
    public function __construct(ParcelItem ...$items)
    {
        foreach ($items as $item) {
            $this->_items[] = clone $item;
        }
    }

    /**
     * @param ParcelItem $item
     * @return ParcelItemCollection
     */
    public function addItem(ParcelItem $item): ParcelItemCollection
    {
        $this->_items[] = $item;
        return $this;
    }

    /**
     * @param array $values
     * @return ParcelItemCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($item) {
            if (is_array($item)) {
                return ParcelItem::fromArray($item);
            } elseif ($item instanceof ParcelItem) {
                return clone $item;
            }
            return null;
        }, $values)));
    }

    public function jsonSerialize()
    {
        return $this->_items;
    }
}
