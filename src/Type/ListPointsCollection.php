<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use IteratorAggregate;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ListPointsCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ListPointsCollection implements IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ListPointsItem[] */
    protected $_items = [];

    /**
     * ListPointsCollection constructor.
     * @param ListPointsItem[] $items
     */
    public function __construct(ListPointsItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ListPointsCollection
     */
    public static function fromArray(array $values): ListPointsCollection
    {
        return new self(...array_map(function ($i) {
            return ListPointsItem::fromArray($i);
        }, $values));
    }

    /**
     * @return ListPointsItem[]
     */
    public function getArray(): array
    {
        return $this->_items;
    }
}
