<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class OrdersBalanceItem
 * @package SergeR\BoxberrySDK\Type
 */
class OrdersBalanceItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    /** @var string */
    protected $Id = '';

    /** @var string */
    protected $Status = '';

    /** @var float */
    protected $Price = 0.0;

    /** @var float */
    protected $DeliverySum = 0.0;

    protected $PaymentSum = 0.0;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param string $ID
     * @return OrdersBalanceItem
     */
    protected function setID($ID)
    {
        $this->Id = (string)$ID;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     * @return OrdersBalanceItem
     */
    protected function setStatus($Status)
    {
        $this->Status = (string)$Status;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->Price;
    }

    /**
     * @param float $Price
     * @return OrdersBalanceItem
     */
    protected function setPrice($Price)
    {
        $this->Price = $this->_floatval($Price);
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliverySum()
    {
        return $this->DeliverySum;
    }

    /**
     * @param float $DeliverySum
     * @return OrdersBalanceItem
     */
    protected function setDeliverySum($DeliverySum)
    {
        $this->DeliverySum = $this->_floatval($DeliverySum);
        return $this;
    }

    /**
     * @return float
     */
    public function getPaymentSum()
    {
        return $this->PaymentSum;
    }

    /**
     * @param float $PaymentSum
     * @return OrdersBalanceItem
     */
    protected function setPaymentSum($PaymentSum)
    {
        $this->PaymentSum = $this->_floatval($PaymentSum);
        return $this;
    }
}