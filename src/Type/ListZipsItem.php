<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class ListZipsItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListZipsItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    /** @var string */
    protected $Zip = '';

    /** @var string */
    protected $City = '';

    /** @var string */
    protected $Region = '';

    /** @var string */
    protected $Area = '';

    /** @var string */
    protected $ZoneExpressDelivery = '';

    /** @var bool */
    protected $ExpressDelivery = true;

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->Zip;
    }

    /**
     * @param string $Zip
     * @return ListZipsItem
     */
    public function setZip($Zip)
    {
        $this->Zip = $Zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     * @return ListZipsItem
     */
    public function setCity($City)
    {
        $this->City = $City;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * @param string $Region
     * @return ListZipsItem
     */
    public function setRegion($Region)
    {
        $this->Region = $Region;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->Area;
    }

    /**
     * @param string $Area
     * @return ListZipsItem
     */
    public function setArea($Area)
    {
        $this->Area = $Area;
        return $this;
    }

    /**
     * @return string
     */
    public function getZoneExpressDelivery()
    {
        return $this->ZoneExpressDelivery;
    }

    /**
     * @param string $ZoneExpressDelivery
     * @return ListZipsItem
     */
    public function setZoneExpressDelivery($ZoneExpressDelivery)
    {
        $this->ZoneExpressDelivery = $ZoneExpressDelivery;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExpressDelivery()
    {
        return $this->ExpressDelivery;
    }

    /**
     * @param bool|string|null $ExpressDelivery
     * @return ListZipsItem
     */
    public function setExpressDelivery($ExpressDelivery)
    {
        $this->ExpressDelivery = $this->_boolval($ExpressDelivery);
        return $this;
    }
}