<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use IteratorAggregate;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

class ListCitiesCollection implements IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var  ListCitiesItem[] */
    protected $_items = [];

    /**
     * ListCitiesCollection constructor.
     * @param ListCitiesItem[] $items
     */
    public function __construct(ListCitiesItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return $this
     */
    public static function fromArray(array $values): ListCitiesCollection
    {
        return new self(...array_map(function ($c) {
            return ListCitiesItem::fromArray($c);
        }, $values));
    }

    /**
     * @return ListCitiesItem[]
     */
    public function getArray(): array
    {
        return $this->_items;
    }
}
