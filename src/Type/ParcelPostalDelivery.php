<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

/**
 * Class ParcelPostalDelivery
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelPostalDelivery extends ParcelDelivery implements \JsonSerializable
{
    /** @var string|null */
    protected $type;

    /** @var string|null */
    protected $fragile;

    /** @var string|null */
    protected $strong;

    /** @var string|null */
    protected $optimize;

    /** @var int|null */
    protected $packing_type;

    /** @var bool|null */
    protected $packing_strict;

    /**
     * @param string|null $type
     * @return ParcelPostalDelivery
     */
    public function setType(?string $type): ParcelPostalDelivery
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param mixed|null $fragile
     * @return ParcelPostalDelivery
     */
    public function setFragile($fragile): ParcelPostalDelivery
    {
        if ($fragile !== null) $fragile = $fragile ? '1' : '0';

        $this->fragile = $fragile;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFragile(): ?string
    {
        return $this->fragile;
    }

    /**
     * @param mixed|null $strong
     * @return ParcelPostalDelivery
     */
    public function setStrong($strong): ParcelPostalDelivery
    {
        if ($strong !== null)
            $strong = $strong ? '1' : '0';

        $this->strong = $strong;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStrong(): ?string
    {
        return $this->strong;
    }

    /**
     * @param mixed|null $optimize
     * @return ParcelPostalDelivery
     */
    public function setOptimize($optimize): ParcelPostalDelivery
    {
        if ($optimize !== null)
            $optimize = $optimize ? '1' : '0';

        $this->optimize = $optimize;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOptimize(): ?string
    {
        return $this->optimize;
    }

    /**
     * @param int|null $packing_type
     * @return ParcelPostalDelivery
     */
    public function setPackingType(?int $packing_type): ParcelPostalDelivery
    {
        $this->packing_type = $packing_type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPackingType(): ?int
    {
        return $this->packing_type;
    }

    /**
     * @param bool|null $packing_strict
     * @return ParcelPostalDelivery
     */
    public function setPackingStrict(?bool $packing_strict): ParcelPostalDelivery
    {
        $this->packing_strict = $packing_strict;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPackingStrict(): ?bool
    {
        return $this->packing_strict;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $data = parent::jsonSerialize();
        foreach (['type', 'fragile', 'strong', 'optimize'] as $property) {
            if ($this->{$property} !== null) $data[$property] = $this->{$property};

            if ($property === 'packing_type') $data[$property] = (string)$data[$property];
        }

        // В описании API ошибка
        if (null !== $this->getPackingType()) $data['packing_type'] = (string)$this->getPackingType();
        if (null !== $this->getPackingStrict()) $data['packing_strict'] = $this->getPackingStrict() ? "1" : "0";

        return $data;
    }
}
