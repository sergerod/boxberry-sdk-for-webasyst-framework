<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */
declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ListStatusesItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListStatusesItem implements FillableFromArray
{
    use MapFromArray;

    /** @var \DateTimeImmutable */
    protected $Date;

    /** @var string */
    protected $Name = '';

    /** @var string */
    protected $Comment = '';

    /**
     * ListStatusesItem constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->Date = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->Date;
    }

    /**
     * @param \DateTimeImmutable|string|null $Date
     * @return ListStatusesItem
     * @throws \Exception
     */
    public function setDate($Date): ListStatusesItem
    {
        if ($Date === null) {
            $Date = new \DateTimeImmutable();
        } elseif (is_string($Date)) {
            $Date = new \DateTimeImmutable($Date);
        }

        $this->Date = clone $Date;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ListStatusesItem
     */
    public function setName(string $Name): ListStatusesItem
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->Comment;
    }

    /**
     * @param string $Comment
     * @return ListStatusesItem
     */
    public function setComment(string $Comment): ListStatusesItem
    {
        $this->Comment = $Comment;
        return $this;
    }
}
