<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class ParselStoryItem
 * @package SergeR\BoxberrySDK\Type
 */
class ParselStoryItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    protected $Track = '';

    protected $Label = '';

    /** @var \DateTimeInterface */
    protected $Date;

    protected $Send = false;

    protected $Barcode = '';

    protected $Imid = '';

    /**
     * ParselStoryItem constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->Date = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getTrack()
    {
        return $this->Track;
    }

    /**
     * @param string $Track
     * @return ParselStoryItem
     */
    public function setTrack($Track)
    {
        $this->Track = (string)$Track;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->Label;
    }

    /**
     * @param string $Label
     * @return ParselStoryItem
     */
    public function setLabel($Label)
    {
        $this->Label = (string)$Label;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDate()
    {
        return clone $this->Date;
    }

    /**
     * @param \DateTimeInterface|string $Date
     * @return ParselStoryItem
     * @throws \Exception
     */
    public function setDate($Date)
    {
        $Date = $this->_date($Date);
        $this->Date = clone $Date;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSend()
    {
        return $this->Send;
    }

    /**
     * @param bool $Send
     * @return ParselStoryItem
     */
    public function setSend($Send)
    {
        $this->Send = (bool)$Send;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->Barcode;
    }

    /**
     * @param string $Barcode
     * @return ParselStoryItem
     */
    public function setBarcode($Barcode)
    {
        $this->Barcode = (string)$Barcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getImid()
    {
        return $this->Imid;
    }

    /**
     * @param string $Imid
     * @return ParselStoryItem
     */
    public function setImid($Imid)
    {
        $this->Imid = (string)$Imid;

        return $this;
    }
}