<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class ListServicesItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListServicesItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    /** @var string */
    protected $Name = '';

    /** @var float */
    protected $Sum = 0.0;

    /** @var DateTimeImmutable */
    protected $Date;

    /** @var string */
    protected $PaymentMethod = '';

    /**
     * ListServicesItem constructor.
     */
    public function __construct()
    {
        $this->Date = date_create_immutable();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ListServicesItem
     */
    public function setName($Name)
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->Sum;
    }

    /**
     * @param float $Sum
     * @return ListServicesItem
     */
    public function setSum($Sum)
    {
        try {
            $this->Sum = $this->_floatval($Sum);
        } catch (InvalidArgumentException $e) {
            $this->Sum = 0.0;
        }
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDate()
    {
        return $this->Date;
    }

    /**
     * @param DateTimeImmutable|string|null $Date
     * @return ListServicesItem
     * @throws Exception
     */
    public function setDate($Date)
    {
        if ($Date === null) {
            $Date = date_create_immutable();
        } elseif (is_string($Date) || is_int($Date)) {
            try {
                $Date = new DateTimeImmutable($Date);
            } catch (Exception $e) {
                $Date = date_create_immutable();
            }
        }
        $this->Date = clone $Date;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }

    /**
     * @param string $PaymentMethod
     * @return ListServicesItem
     */
    public function setPaymentMethod($PaymentMethod)
    {
        $this->PaymentMethod = $PaymentMethod;
        return $this;
    }
}