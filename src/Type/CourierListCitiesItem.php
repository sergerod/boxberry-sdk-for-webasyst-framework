<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class CourierListCitiesItem
 * @package SergeR\BoxberrySDK\Type
 */
class CourierListCitiesItem implements FillableFromArray
{
    use MapFromArray;

    /** @var string */
    protected $City = '';

    /** @var string */
    protected $Region = '';

    /** @var string */
    protected $Area = '';

    /** @var int|null */
    protected $DeliveryPeriod;

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     * @return CourierListCitiesItem
     */
    public function setCity($City)
    {
        $this->City = $City;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * @param string $Region
     * @return CourierListCitiesItem
     */
    public function setRegion($Region)
    {
        $this->Region = $Region;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->Area;
    }

    /**
     * @param string $Area
     * @return CourierListCitiesItem
     */
    public function setArea($Area)
    {
        $this->Area = $Area;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeliveryPeriod()
    {
        return $this->DeliveryPeriod;
    }

    /**
     * @param int|null|string $DeliveryPeriod
     * @return CourierListCitiesItem
     */
    public function setDeliveryPeriod($DeliveryPeriod)
    {
        if ($DeliveryPeriod !== null) {
            $DeliveryPeriod = (int)$DeliveryPeriod;
        }
        $this->DeliveryPeriod = $DeliveryPeriod;
        return $this;
    }
}