<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use DateTimeInterface;
use Exception;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class ParselSendStoryItem
 * @package SergeR\BoxberrySDK\Type
 */
class ParselSendStoryItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    /** @var string */
    protected $Track = '';

    /** @var string */
    protected $Label = '';

    /** @var DateTimeInterface */
    protected $Date;

    /**
     * ParselSendStoryItem constructor.
     */
    public function __construct()
    {
        $this->Date = date_create_immutable();
    }

    /**
     * @return string
     */
    public function getTrack()
    {
        return $this->Track;
    }

    /**
     * @return array
     */
    public function getTracks()
    {
        return array_map('trim', (array)explode(',', $this->getTrack()));
    }

    /**
     * @param string $Track
     * @return $this
     */
    public function setTrack($Track)
    {
        $this->Track = $Track;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->Label;
    }

    /**
     * @param string $Label
     * @return $this
     */
    public function setLabel($Label)
    {
        $this->Label = $Label;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDate()
    {
        return $this->Date;
    }

    /**
     * @param DateTimeInterface $Date
     * @return $this
     */
    public function setDate($Date)
    {
        try {
            $Date = $this - $this->_date($Date);
        } catch (Exception $e) {
            $Date = date_create_immutable();
        }
        $this->Date = clone $Date;
        return $this;
    }
}