<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class PartialDeliveryItem
 * @package SergeR\BoxberrySDK\Type
 */
class PartialDeliveryItem implements FillableFromArray
{
    use MapFromArray;

    /** @var string */
    protected $Name='';

    /** @var int */
    protected $Give=0;

    /** @var int */
    protected $Return=0;

    /** @var string */
    protected $ItemID='';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return int
     */
    public function getGive()
    {
        return $this->Give;
    }

    /**
     * @param int $Give
     */
    public function setGive($Give)
    {
        $this->Give = (int)$Give;
    }

    /**
     * @return int
     */
    public function getReturn()
    {
        return $this->Return;
    }

    /**
     * @param int $Return
     */
    public function setReturn($Return)
    {
        $this->Return = (int)$Return;
    }

    /**
     * @return string
     */
    public function getItemID()
    {
        return $this->ItemID;
    }

    /**
     * @param string $ItemID
     */
    public function setItemID($ItemID)
    {
        $this->ItemID = $ItemID;
    }
}