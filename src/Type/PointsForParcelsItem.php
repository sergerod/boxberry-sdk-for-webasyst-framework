<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class PointsForParcelsItem
 * @package SergeR\BoxberrySDK\Type
 */
class PointsForParcelsItem implements FillableFromArray
{
    use MapFromArray;

    /** @var string */
    protected $Code = '';

    /** @var string */
    protected $Name = '';

    /** @var string */
    protected $City = '';

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param string $Code
     * @return PointsForParcelsItem
     */
    public function setCode($Code)
    {
        $this->Code = (string)$Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return PointsForParcelsItem
     */
    public function setName($Name)
    {
        $this->Name = (string)$Name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     * @return PointsForParcelsItem
     */
    public function setCity($City)
    {
        $this->City = (string)$City;
        return $this;
    }
}