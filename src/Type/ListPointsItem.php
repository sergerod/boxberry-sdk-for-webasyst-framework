<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class ListPointsItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListPointsItem implements FillableFromArray
{
    use MapFromArray, Typecast;

    /** @var string */
    protected $Code = '';

    /** @var string */
    protected $Name = '';

    /** @var string */
    protected $Address = '';

    /** @var string */
    protected $Phone = '';

    /** @var string */
    protected $WorkShedule = '';

    /** @var string */
    protected $TripDescription = '';

    /** @var int */
    protected $DeliveryPeriod = 0;

    /** @var string */
    protected $CityCode = '';

    /** @var string */
    protected $CityName = '';

    /** @var string */
    protected $TariffZone = '';

    /** @var string */
    protected $Settlement = '';

    /** @var string */
    protected $Area = '';

    /** @var string */
    protected $Country = '';

    /** @var bool */
    protected $OnlyPrepaidOrders = true;

    /** @var string */
    protected $AddressReduce = '';

    /** @var bool */
    protected $Acquiring = false;

    /** @var bool */
    protected $DigitalSignature = false;

    /** @var string */
    protected $TypeOfOffice = '';

    /** @var bool */
    protected $NalKD = false;

    /** @var string */
    protected $Metro = '';

    /** @var null|float */
    protected $VolumeLimit = null;

    /** @var null|int */
    protected $LoadLimit = null;

    /** @var string */
    protected $GPS = '';

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->Code;
    }

    /**
     * @param string $Code
     * @return ListPointsItem
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ListPointsItem
     */
    public function setName($Name)
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @param string $Address
     * @return ListPointsItem
     */
    public function setAddress($Address)
    {
        $this->Address = $Address;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @param string $Phone
     * @return ListPointsItem
     */
    public function setPhone($Phone)
    {
        $this->Phone = $Phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getWorkShedule(): string
    {
        return $this->WorkShedule;
    }

    /**
     * @param string $WorkShedule
     * @return ListPointsItem
     */
    public function setWorkShedule($WorkShedule): ListPointsItem
    {
        $this->WorkShedule = $WorkShedule;
        return $this;
    }

    /**
     * @return string
     */
    public function getTripDescription(): string
    {
        return $this->TripDescription;
    }

    /**
     * @param string $TripDescription
     * @return ListPointsItem
     */
    public function setTripDescription(string $TripDescription): ListPointsItem
    {
        $this->TripDescription = $TripDescription;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryPeriod()
    {
        return $this->DeliveryPeriod;
    }

    /**
     * @param int $DeliveryPeriod
     * @return ListPointsItem
     */
    public function setDeliveryPeriod($DeliveryPeriod)
    {
        $this->DeliveryPeriod = (int)$DeliveryPeriod;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityCode()
    {
        return $this->CityCode;
    }

    /**
     * @param string $CityCode
     * @return ListPointsItem
     */
    public function setCityCode($CityCode)
    {
        $this->CityCode = $CityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->CityName;
    }

    /**
     * @param string $CityName
     * @return ListPointsItem
     */
    public function setCityName($CityName)
    {
        $this->CityName = $CityName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTariffZone()
    {
        return $this->TariffZone;
    }

    /**
     * @param string $TariffZone
     * @return ListPointsItem
     */
    public function setTariffZone($TariffZone)
    {
        $this->TariffZone = $TariffZone;
        return $this;
    }

    /**
     * @return string
     */
    public function getSettlement()
    {
        return $this->Settlement;
    }

    /**
     * @param string $Settlement
     * @return ListPointsItem
     */
    public function setSettlement($Settlement)
    {
        $this->Settlement = $Settlement;
        return $this;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->Area;
    }

    /**
     * @param string $Area
     * @return ListPointsItem
     */
    public function setArea($Area)
    {
        $this->Area = $Area;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param string $Country
     * @return ListPointsItem
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOnlyPrepaidOrders()
    {
        return $this->OnlyPrepaidOrders;
    }

    /**
     * @param bool $OnlyPrepaidOrders
     * @return ListPointsItem
     */
    public function setOnlyPrepaidOrders($OnlyPrepaidOrders)
    {
        $this->OnlyPrepaidOrders = $this->_boolval($OnlyPrepaidOrders);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressReduce()
    {
        return $this->AddressReduce;
    }

    /**
     * @param string $AddressReduce
     * @return ListPointsItem
     */
    public function setAddressReduce($AddressReduce)
    {
        $this->AddressReduce = $AddressReduce;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAcquiring()
    {
        return $this->Acquiring;
    }

    /**
     * @param bool $Acquiring
     * @return ListPointsItem
     */
    public function setAcquiring($Acquiring)
    {
        $this->Acquiring = $this->_boolval($Acquiring);
        return $this;
    }

    /**
     * @return bool
     */
    public function isDigitalSignature()
    {
        return $this->DigitalSignature;
    }

    /**
     * @param bool $DigitalSignature
     * @return ListPointsItem
     */
    public function setDigitalSignature($DigitalSignature)
    {
        $this->DigitalSignature = $this->_boolval($DigitalSignature);
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeOfOffice()
    {
        return $this->TypeOfOffice;
    }

    /**
     * @param string $TypeOfOffice
     * @return ListPointsItem
     */
    public function setTypeOfOffice($TypeOfOffice)
    {
        $this->TypeOfOffice = $TypeOfOffice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNalKD()
    {
        return $this->NalKD;
    }

    /**
     * @param bool $NalKD
     * @return ListPointsItem
     */
    public function setNalKD($NalKD)
    {
        $this->NalKD = $this->_boolval($NalKD);
        return $this;
    }

    /**
     * @return string
     */
    public function getMetro()
    {
        return $this->Metro;
    }

    /**
     * @param string $Metro
     * @return ListPointsItem
     */
    public function setMetro($Metro)
    {
        $this->Metro = $Metro;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getVolumeLimit(): ?float
    {
        return $this->VolumeLimit;
    }

    /**
     * @param float|null $VolumeLimit
     * @return ListPointsItem
     */
    public function setVolumeLimit($VolumeLimit)
    {
        if (is_string($VolumeLimit) && ($VolumeLimit === '')) {
            $VolumeLimit = null;
        }
        $this->VolumeLimit = $VolumeLimit === null ?: (float)$VolumeLimit;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLoadLimit(): ?int
    {
        return $this->LoadLimit;
    }

    /**
     * @param int|null $LoadLimit
     * @return ListPointsItem
     */
    public function setLoadLimit($LoadLimit)
    {
        if (is_string($LoadLimit) && ($LoadLimit === '')) {
            $LoadLimit = null;
        }
        $this->LoadLimit = $LoadLimit === null ?: (int)$LoadLimit;
        return $this;
    }

    /**
     * @return string
     */
    public function getGPS()
    {
        return $this->GPS;
    }

    /**
     * @param string $GPS
     * @return ListPointsItem
     */
    public function setGPS($GPS)
    {
        $this->GPS = $GPS;
        return $this;
    }
}
