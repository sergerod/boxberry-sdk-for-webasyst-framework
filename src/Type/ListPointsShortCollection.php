<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use DateTimeImmutable;
use Exception;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ListPointsShortCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ListPointsShortCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ListPointsShortItem[] */
    protected $_items = [];

    /**
     * ListPointsShortCollection constructor.
     * @param ListPointsShortItem[] $items
     */
    public function __construct(ListPointsShortItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ListPointsShortCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_map(function (array $i) {
            try {
                $item = ListPointsShortItem::fromArray($i);
            } catch (Exception $e) {
                $i['UpdateDate'] = new DateTimeImmutable();
                $item = ListPointsShortItem::fromArray($i);
            }
            return $item;
        }, $values));
    }
}