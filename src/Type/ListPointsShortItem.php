<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use DateTimeImmutable;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ListPointsShortItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListPointsShortItem implements FillableFromArray
{
    use MapFromArray;

    /** @var string */
    protected $CityCode = '';

    /** @var string */
    protected $Code = '';

    /** @var DateTimeImmutable */
    protected $UpdateDate;

    /**
     * ListPointsShortItem constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->UpdateDate = date_create_immutable();
    }

    /**
     * @return string
     */
    public function getCityCode()
    {
        return $this->CityCode;
    }

    /**
     * @param string $CityCode
     * @return ListPointsShortItem
     */
    public function setCityCode($CityCode)
    {
        $this->CityCode = $CityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param string $Code
     * @return ListPointsShortItem
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdateDate()
    {
        return $this->UpdateDate;
    }

    /**
     * @param DateTimeImmutable|string|null $UpdateDate
     * @return ListPointsShortItem
     */
    public function setUpdateDate($UpdateDate)
    {
        if ($UpdateDate === null) {
            $UpdateDate = date_create_immutable();
        } elseif (is_string($UpdateDate) || is_int($UpdateDate)) {
            $UpdateDate = date_create_immutable($UpdateDate);
        }

        $this->UpdateDate = $UpdateDate;
        return $this;
    }
}