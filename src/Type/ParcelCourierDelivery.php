<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2023
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ParcelCourierDelivery
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelCourierDelivery extends ParcelDelivery implements FillableFromArray, \JsonSerializable
{
    use MapFromArray;

    /** @var \DateTimeInterface|null */
    protected $Timesfrom1 = null;

    /** @var \DateTimeInterface|null */
    protected $Timesto1 = null;

    /** @var \DateTimeInterface|null */
    protected $Timesfrom2 = null;

    /** @var \DateTimeInterface|null */
    protected $Timesto2 = null;

    /** @var string */
    protected $Timep = '';

    /** @var \DateTimeInterface|null */
    protected $DeliveryDate = null;

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimesfrom1(): ?\DateTimeInterface
    {
        return $this->Timesfrom1;
    }

    /**
     * @param \DateTimeInterface|null $Timesfrom1
     * @return ParcelCourierDelivery
     */
    public function setTimesfrom1(?\DateTimeInterface $Timesfrom1): ParcelCourierDelivery
    {
        $this->Timesfrom1 = $Timesfrom1;
        return $this;
    }

    public function getTimesto1(): ?\DateTimeInterface
    {
        return $this->Timesto1;
    }

    /**
     * @param ?\DateTimeInterface $Timesto1
     * @return ParcelCourierDelivery
     */
    public function setTimesto1(?\DateTimeInterface $Timesto1): ParcelCourierDelivery
    {
        $this->Timesto1 = $Timesto1;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimesfrom2(): ?\DateTimeInterface
    {
        return $this->Timesfrom2;
    }

    /**
     * @param \DateTimeInterface|null $Timesfrom2
     * @return ParcelCourierDelivery
     */
    public function setTimesfrom2(?\DateTimeInterface $Timesfrom2): ParcelCourierDelivery
    {
        $this->Timesfrom2 = $Timesfrom2;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimesto2(): ?\DateTimeInterface
    {
        return $this->Timesto2;
    }

    /**
     * @param \DateTimeInterface|null $Timesto2
     * @return ParcelCourierDelivery
     */
    public function setTimesto2(?\DateTimeInterface $Timesto2): ParcelCourierDelivery
    {
        $this->Timesto2 = $Timesto2;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimep(): string
    {
        return $this->Timep;
    }

    /**
     * @param string $Timep
     * @return ParcelCourierDelivery
     */
    public function setTimep(string $Timep)
    {
        $this->Timep = $Timep;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->DeliveryDate;
    }

    /**
     * @param \DateTimeInterface $DeliveryDate
     * @return ParcelCourierDelivery
     * @throws \Exception
     */
    public function setDeliveryDate(\DateTimeInterface $DeliveryDate): ParcelCourierDelivery
    {
        $this->DeliveryDate = $DeliveryDate;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $data = parent::jsonSerialize();
        if ($this->getDeliveryDate())
            $data['delivery_date'] = $this->getDeliveryDate()->format('Y-m-d');

        if ($this->getTimesfrom1())
            $data['timesfrom1'] = $this->getTimesfrom1()->format('H:i');
        if ($this->getTimesto1())
            $data['timesto1'] = $this->getTimesto1()->format('H:i');
        if ($this->getTimesfrom2())
            $data['timesfrom2'] = $this->getTimesfrom2()->format('H:i');
        if ($this->getTimesto2())
            $data['timesto2'] = $this->getTimesto2()->format('H:i');

        if ($comment = trim((string)$this->getComentk()))
            $data['comentk'] = $comment;

        return $data;
    }
}
