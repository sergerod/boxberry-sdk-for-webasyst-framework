<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ListCitiesItem
 * @package SergeR\BoxberrySDK\Type
 */
class ListCitiesItem implements FillableFromArray
{
    use MapFromArray;

    /**
     * @var string
     */
    protected $Code = '';

    /**
     * @var string
     */
    protected $Name = '';

    /**
     * @var bool
     */
    protected $ReceptionLaP = false;

    /**
     * @var bool
     */
    protected $DeliveryLaP = false;

    /**
     * @var bool
     */
    protected $Reception = false;

    /**
     * @var bool
     */
    protected $PickupPoint = false;

    /**
     * @var bool
     */
    protected $CourierDelivery = false;

    /**
     * @var bool
     */
    protected $ForeignReceptionReturns = false;

    /**
     * @var bool
     */
    protected $Terminal = false;

    /**
     * @var string
     */
    protected $Kladr = '';

    /**
     * @var string
     */
    protected $Region = '';

    /**
     * @var string
     */
    protected $CountryCode = '';

    /**
     * @var string
     */
    protected $UniqName = '';

    /**
     * @var string
     */
    protected $District = '';

    /**
     * @var string
     */
    protected $Prefix = '';

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param string $Code
     * @return ListCitiesItem
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ListCitiesItem
     */
    public function setName($Name)
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReceptionLaP()
    {
        return $this->ReceptionLaP;
    }

    /**
     * @param bool|string|int $ReceptionLaP
     * @return ListCitiesItem
     */
    public function setReceptionLaP($ReceptionLaP)
    {
        $this->ReceptionLaP = (bool)$ReceptionLaP;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryLaP()
    {
        return $this->DeliveryLaP;
    }

    /**
     * @param bool|string|int $DeliveryLaP
     * @return ListCitiesItem
     */
    public function setDeliveryLaP($DeliveryLaP)
    {
        $this->DeliveryLaP = (bool)$DeliveryLaP;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReception()
    {
        return $this->Reception;
    }

    /**
     * @param bool|string|int $Reception
     * @return ListCitiesItem
     */
    public function setReception($Reception)
    {
        $this->Reception = (bool)$Reception;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPickupPoint()
    {
        return $this->PickupPoint;
    }

    /**
     * @param bool|string|int $PickupPoint
     * @return ListCitiesItem
     */
    public function setPickupPoint($PickupPoint)
    {
        $this->PickupPoint = (bool)$PickupPoint;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCourierDelivery()
    {
        return $this->CourierDelivery;
    }

    /**
     * @param bool|string|int $CourierDelivery
     * @return ListCitiesItem
     */
    public function setCourierDelivery($CourierDelivery)
    {
        $this->CourierDelivery = (bool)$CourierDelivery;
        return $this;
    }

    /**
     * @return bool
     */
    public function isForeignReceptionReturns()
    {
        return $this->ForeignReceptionReturns;
    }

    /**
     * @param bool|string|int $ForeignReceptionReturns
     * @return ListCitiesItem
     */
    public function setForeignReceptionReturns($ForeignReceptionReturns)
    {
        $this->ForeignReceptionReturns = (bool)$ForeignReceptionReturns;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTerminal()
    {
        return $this->Terminal;
    }

    /**
     * @param bool|string|int $Terminal
     * @return ListCitiesItem
     */
    public function setTerminal($Terminal)
    {
        $this->Terminal = (bool)$Terminal;
        return $this;
    }

    /**
     * @return string
     */
    public function getKladr()
    {
        return $this->Kladr;
    }

    /**
     * @param string $Kladr
     * @return ListCitiesItem
     */
    public function setKladr($Kladr)
    {
        $this->Kladr = $Kladr;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * @param string $Region
     * @return ListCitiesItem
     */
    public function setRegion($Region)
    {
        $this->Region = $Region;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->CountryCode;
    }

    /**
     * @param string $CountryCode
     * @return ListCitiesItem
     */
    public function setCountryCode($CountryCode)
    {
        $this->CountryCode = $CountryCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUniqName()
    {
        return $this->UniqName;
    }

    /**
     * @param string $UniqName
     * @return ListCitiesItem
     */
    public function setUniqName($UniqName)
    {
        $this->UniqName = $UniqName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->District;
    }

    /**
     * @param string $District
     * @return ListCitiesItem
     */
    public function setDistrict($District)
    {
        $this->District = $District;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->Prefix;
    }

    /**
     * @param string $Prefix
     * @return ListCitiesItem
     */
    public function setPrefix($Prefix)
    {
        $this->Prefix = $Prefix;
        return $this;
    }
}