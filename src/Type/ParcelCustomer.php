<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ParcelCustomer
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelCustomer implements FillableFromArray, \JsonSerializable
{
    use MapFromArray;

    protected $Fio;
    protected $Phone;
    protected $Phone2;
    protected $Email;
    protected $Name;
    protected $Address;
    protected $Inn;
    protected $Kpp;
    protected $RS;
    protected $Bank;
    protected $KorS;
    protected $Bik;

    /**
     * @return string
     */
    public function getFio()
    {
        return $this->Fio;
    }

    /**
     * @param string $Fio
     * @return ParcelCustomer
     */
    public function setFio($Fio)
    {
        $this->Fio = $Fio;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @param string $Phone
     * @return ParcelCustomer
     */
    public function setPhone($Phone)
    {
        $this->Phone = $Phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone2()
    {
        return $this->Phone2;
    }

    /**
     * @param string $Phone2
     * @return ParcelCustomer
     */
    public function setPhone2($Phone2)
    {
        $this->Phone2 = $Phone2;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @param string $Email
     * @return ParcelCustomer
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ParcelCustomer
     */
    public function setName($Name)
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @param string $Address
     * @return ParcelCustomer
     */
    public function setAddress($Address)
    {
        $this->Address = $Address;
        return $this;
    }

    /**
     * @return string
     */
    public function getInn()
    {
        return $this->Inn;
    }

    /**
     * @param string $Inn
     * @return ParcelCustomer
     */
    public function setInn($Inn)
    {
        $this->Inn = $Inn;
        return $this;
    }

    /**
     * @return string
     */
    public function getKpp()
    {
        return $this->Kpp;
    }

    /**
     * @param string $Kpp
     * @return ParcelCustomer
     */
    public function setKpp($Kpp)
    {
        $this->Kpp = $Kpp;
        return $this;
    }

    /**
     * @return string
     */
    public function getRS()
    {
        return $this->RS;
    }

    /**
     * @param string $RS
     * @return ParcelCustomer
     */
    public function setRS($RS)
    {
        $this->RS = $RS;
        return $this;
    }

    /**
     * @return string
     */
    public function getBank()
    {
        return $this->Bank;
    }

    /**
     * @param string $Bank
     * @return ParcelCustomer
     */
    public function setBank($Bank)
    {
        $this->Bank = $Bank;
        return $this;
    }

    /**
     * @return string
     */
    public function getKorS()
    {
        return $this->KorS;
    }

    /**
     * @param string $KorS
     * @return ParcelCustomer
     */
    public function setKorS($KorS)
    {
        $this->KorS = $KorS;
        return $this;
    }

    /**
     * @return ?string
     */
    public function getBik(): ?string
    {
        return $this->Bik;
    }

    /**
     * @param string|null $Bik
     * @return ParcelCustomer
     */
    public function setBik(?string $Bik): ParcelCustomer
    {
        $this->Bik = $Bik;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_filter([
            'fio'     => (string)$this->getFio(),
            'phone'   => (string)$this->getPhone(),
            'phone2'  => $this->getPhone2(),
            'email'   => $this->getEmail(),
            'name'    => $this->getName(),
            'address' => $this->getAddress(),
            'inn'     => $this->getInn(),
            'kpp'     => $this->getKpp(),
            'r_s'     => $this->getRS(),
            'bank'    => $this->getBank(),
            'kar_s'   => $this->getKorS(),
            'bik'     => $this->getBik()
        ], function ($v) {
            return $v !== null;
        });
    }
}
