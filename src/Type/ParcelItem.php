<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ParcelItem
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelItem implements FillableFromArray, \JsonSerializable
{
    use MapFromArray;

    protected $Id = '';
    protected $Name = '';
    protected $UnitName = '';
    protected $Nds = '';
    protected $Price = 0.0;
    protected $Quantity = 0;

    public function jsonSerialize(): array
    {
        $data = [
            'name'     => $this->getName(),
            'nds'      => $this->getNds(),
            'price'    => number_format($this->getPrice(), 2, '.', ''),
            'quantity' => (string)$this->getQuantity()
        ];
        if ($unit_name = trim($this->getUnitName())) $data['UnitName'] = $unit_name;
        if ($id = trim($this->getId())) $data['id'] = $id;

        return $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     * @return ParcelItem
     */
    public function setName(string $Name): ParcelItem
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNds(): string
    {
        return $this->Nds;
    }

    /**
     * @param string $Nds
     * @return ParcelItem
     */
    public function setNds(string $Nds): ParcelItem
    {
        $this->Nds = $Nds;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->Price;
    }

    /**
     * @param float $Price
     * @return ParcelItem
     */
    public function setPrice(float $Price): ParcelItem
    {
        $this->Price = $Price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->Quantity;
    }

    /**
     * @param int $Quantity
     * @return ParcelItem
     */
    public function setQuantity(int $Quantity): ParcelItem
    {
        $this->Quantity = $Quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnitName(): string
    {
        return $this->UnitName;
    }

    /**
     * @param string $UnitName
     * @return ParcelItem
     */
    public function setUnitName(string $UnitName): ParcelItem
    {
        $this->UnitName = $UnitName;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->Id;
    }

    /**
     * @param string $Id
     * @return ParcelItem
     */
    public function setId(string $Id): ParcelItem
    {
        $this->Id = $Id;
        return $this;
    }
}
