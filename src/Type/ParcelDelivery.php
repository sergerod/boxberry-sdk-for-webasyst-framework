<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

/**
 * Class ParcelDelivery
 * @package SergeR\BoxberrySDK\Type
 */
abstract class ParcelDelivery implements \JsonSerializable
{
    protected $index = '';

    protected $citi = '';

    protected $Addressp = '';

    protected $Comentk = null;

    /**
     * @return string|null
     */
    public function getComentk(): ?string
    {
        return $this->Comentk;
    }

    /**
     * @param string|null $Comentk
     * @return ParcelDelivery
     */
    public function setComentk(?string $Comentk): ParcelDelivery
    {
        $this->Comentk = $Comentk;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressp(): string
    {
        return $this->Addressp;
    }

    /**
     * @param string $Addressp
     * @return ParcelDelivery
     */
    public function setAddressp(string $Addressp): ParcelDelivery
    {
        $this->Addressp = $Addressp;
        return $this;
    }

    /**
     * @param string $index
     * @return ParcelDelivery
     */
    public function setIndex(string $index): ParcelDelivery
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndex(): string
    {
        return $this->index;
    }

    /**
     * @param string $citi
     * @return ParcelDelivery
     */
    public function setCiti(string $citi): ParcelDelivery
    {
        $this->citi = $citi;
        return $this;
    }

    /**
     * @return string
     */
    public function getCiti(): string
    {
        return $this->citi;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $data = array_filter([
            'index' => $this->getIndex(),
            'citi'  => $this->getCiti()
        ]);

        if ($this->getAddressp() !== null) $data['addressp'] = $this->getAddressp();

        return $data;
    }
}
