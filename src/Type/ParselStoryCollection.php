<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ParselStoryCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ParselStoryCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /**
     * @var ParselStoryItem[]
     */
    protected $_items = [];

    /**
     * ParselStoryCollection constructor.
     * @param ParselStoryItem[] $items
     */
    public function __construct(ParselStoryItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ParselStoryCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($item) {
            try {
                return ParselStoryItem::fromArray($item);
            } catch (\Exception $e) {
                return null;
            }
        }, $values)));
    }
}