<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class CourierListCitiesCollection
 * @package SergeR\BoxberrySDK\Type
 */
class CourierListCitiesCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var CourierListCitiesItem[] */
    protected $_items = [];

    /**
     * CourierListCitiesCollection constructor.
     * @param CourierListCitiesItem[] $items
     */
    public function __construct(CourierListCitiesItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return CourierListCitiesCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($i) {
            if ($i instanceof CourierListCitiesItem) {
                return clone $i;
            } elseif (is_array($i)) {
                return CourierListCitiesItem::fromArray($i);
            }
            return null;
        }, $values)));
    }
}