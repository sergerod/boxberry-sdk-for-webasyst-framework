<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class OrdersBalanceCollection
 * @package SergeR\BoxberrySDK\Type
 */
class OrdersBalanceCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var OrdersBalanceItem[] */
    protected $_items = [];

    /**
     * OrdersBalanceCollection constructor.
     * @param OrdersBalanceItem[] $items
     */
    public function __construct(OrdersBalanceItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return OrdersBalanceCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($item) {
            return is_array($item) ? OrdersBalanceItem::fromArray($item) : null;
        }, $values)));
    }
}