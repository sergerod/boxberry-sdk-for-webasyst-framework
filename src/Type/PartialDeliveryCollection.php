<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class PartialDeliveryCollection
 * @package SergeR\BoxberrySDK\Type
 */
class PartialDeliveryCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var PartialDeliveryItem[] */
    protected $_items = [];

    /**
     * PartialDeliveryCollection constructor.
     * @param PartialDeliveryItem[] $items
     */
    public function __construct(PartialDeliveryItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return PartialDeliveryCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($i) {
            if ($i instanceof PartialDeliveryItem) {
                return clone $i;
            } elseif (is_array($i)) {
                return PartialDeliveryItem::fromArray($i);
            }
            return null;
        }, $values)));
    }
}