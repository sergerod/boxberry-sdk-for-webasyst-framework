<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\ArrayToObjectMapper\MapFromArray;

/**
 * Class ParcelPoint
 * @package SergeR\BoxberrySDK\Type
 */
class ParcelPoint implements FillableFromArray, \JsonSerializable
{
    use MapFromArray;

    /** @var string|null */
    protected $Name = '';

    /** @var string|null */
    protected $Name1;

    /**
     * @param ?string $code
     * @return ParcelPoint
     */
    public function setDestinationCode(?string $code): ParcelPoint
    {
        return $this->setName($code);
    }

    /**
     * @return ?string
     */
    public function getDestinationCode(): ?string
    {
        return $this->getName();
    }

    /**
     * @return ?string
     */
    public function getName(): ?string
    {
        return $this->Name;
    }

    /**
     * @param string|null $Name
     * @return ParcelPoint
     */
    public function setName(?string $Name = null): ParcelPoint
    {
        $this->Name = $Name;
        return $this;
    }

    /**
     * @param string|null $code
     * @return ParcelPoint
     */
    public function setReceptionCode(?string $code = null): ParcelPoint
    {
        return $this->setName1($code);
    }

    /**
     * @return string|null
     */
    public function getReceptionCode(): ?string
    {
        return $this->getName1();
    }

    /**
     * @return string|null
     */
    public function getName1(): ?string
    {
        return $this->Name1;
    }

    /**
     * @param string|null $Name1
     * @return ParcelPoint
     */
    public function setName1(?string $Name1 = null): ParcelPoint
    {
        $this->Name1 = $Name1;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_filter(['name' => $this->getName(), 'name1' => $this->getName1()], function ($v) {
            return $v !== null;
        });
    }
}
