<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use Exception;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ListServicesCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ListServicesCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ListServicesItem[] */
    protected $_items = [];

    /**
     * ListServicesCollection constructor.
     * @param ListServicesItem[] $items
     */
    public function __construct(ListServicesItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ListServicesCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($i) {
            if ($i instanceof ListServicesItem) {
                return clone $i;
            } elseif (is_array($i)) {
                try {
                    return ListServicesItem::fromArray($i);
                } catch (Exception $e) {
                    return null;
                }
            }
            return null;
        }, $values)));
    }
}