<?php
/**
 * @author Serge Rodovnichenko <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use Exception;
use IteratorAggregate;
use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ParselSendStoryCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ParselSendStoryCollection implements IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ParselSendStoryItem[] */
    protected $_items = [];

    /**
     * ParselSendStoryCollection constructor.
     * @param ParselSendStoryItem ...$items
     */
    public function __construct(ParselSendStoryItem ...$items)
    {
        $this->_items = $items;
    }

    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($item) {
            try {
                return ParselSendStoryItem::fromArray($item);
            } catch (Exception $e) {
                return null;
            }
        }, $values)));
    }
}