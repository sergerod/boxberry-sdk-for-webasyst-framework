<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Type;

use SergeR\ArrayToObjectMapper\FillableFromArray;
use SergeR\BoxberrySDK\Traits\ArrayIteratorAggregateCollection;

/**
 * Class ListStatusesCollection
 * @package SergeR\BoxberrySDK\Type
 */
class ListStatusesCollection implements \IteratorAggregate, FillableFromArray
{
    use ArrayIteratorAggregateCollection;

    /** @var ListStatusesItem[] */
    protected $_items = [];

    /**
     * ListStatusesCollection constructor.
     * @param ListStatusesItem[] $items
     */
    public function __construct(ListStatusesItem ...$items)
    {
        $this->_items = $items;
    }

    /**
     * @param array $values
     * @return ListStatusesCollection
     */
    public static function fromArray(array $values)
    {
        return new self(...array_filter(array_map(function ($i) {
            try {
                $item = ListStatusesItem::fromArray($i);
            } catch (\Exception $e) {
                $item = null;
            }

            return $item;
        }, $values)));
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function usort(callable $callback): ListStatusesCollection
    {
        usort($this->_items, $callback);
        return $this;
    }

    /**
     * @return ListStatusesItem
     */
    public function end(): ?ListStatusesItem
    {
        return end($this->_items) ?: null;
    }
}
