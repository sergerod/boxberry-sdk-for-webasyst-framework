<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK;

use Psr\Log\LoggerInterface;
use SergeR\BoxberrySDK\Request\AbstractRequest;
use waException;
use waNet;

/**
 * Class WebasystClient
 * @package SergeR\BoxberrySDK
 */
class WebasystClient
{
    /** @var string */
    protected $api_entry_point = 'http://api.boxberry.de/json.php';

    /** @var string */
    protected $token;

    /** @var LoggerInterface */
    protected $logger;

    /** @var array */
    protected $default_wanet_options = [];

    /** @var array */
    protected $default_wanet_headers = [];

    protected $_log_level = null;

    /** @var string|null */
    protected $user_agent;

    /**
     * WebasystClient constructor.
     * @param string $token
     * @param string|null $api
     */
    public function __construct(string $token, ?string $api = null)
    {
        $this->setToken($token);
        if ($api) {
            $this->api_entry_point = $api;
        }
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getApiEntryPoint(): string
    {
        return $this->api_entry_point;
    }

    /**
     * @param string $api_entry_point
     * @return WebasystClient
     */
    public function setApiEntryPoint(string $api_entry_point): WebasystClient
    {
        $this->api_entry_point = $api_entry_point;
        return $this;
    }

    /**
     * @param AbstractRequest $request
     * @param string $method
     * @param array $wanet_options
     * @param array $wanet_headers
     * @return waNet
     * @throws waException
     */
    public function query(AbstractRequest $request, $method = waNet::METHOD_GET, array $wanet_options = [], array $wanet_headers = []): waNet
    {
        $wanet_options = $this->_getWanetOptions($wanet_options);
        $wanet_headers = $this->_getWanetHeaders($wanet_headers);

        $net = new waNet($wanet_options, $wanet_headers);

        if ($this->getUserAgent()) {
            $net->userAgent($this->getUserAgent());
        }

        $content = ['token' => $this->token] + $request->getData();
        $this->_log($this->getLogLevel(), "Request {class}:\n{serialized}", ['class' => get_class($request), 'serialized' => var_export(isset($content['token']) ? ['token' => '*** hidden ***'] + $content : $content, true)]);

        try {
            $net->query($this->api_entry_point, $content, $method);
        } catch (waException $e) {
            $this->_logException($e);
            throw $e;
        }

        if ($this->getLogLevel()) {
            $headers = $net->getResponseHeader();
            $this->_log($this->getLogLevel(), "Response headers:\n{headers}", ['headers' => var_export($headers, true)]);
        }

        return $net;
    }

    /**
     * @param array $custom_options
     * @return array
     */
    protected function _getWanetOptions(array $custom_options = []): array
    {
        return array_merge(
            ['request_format' => waNet::FORMAT_RAW, 'format' => waNet::FORMAT_JSON],
            $this->getDefaultWanetOptions(),
            $custom_options
        );
    }

    /**
     * @return array
     */
    public function getDefaultWanetOptions(): array
    {
        return $this->default_wanet_options;
    }

    /**
     * @param array $default_wanet_options
     * @return $this
     */
    public function setDefaultWanetOptions(array $default_wanet_options): WebasystClient
    {
        $this->default_wanet_options = $default_wanet_options;
        return $this;
    }

    protected function _getWanetHeaders(array $custom_headers = []): array
    {
        return array_merge(
            $this->getDefaultWanetHeaders(),
            $custom_headers
        );
    }

    /**
     * @return array
     */
    public function getDefaultWanetHeaders(): array
    {
        return $this->default_wanet_headers;
    }

    /**
     * @param array $default_wanet_headers
     * @return $this
     */
    public function setDefaultWanetHeaders(array $default_wanet_headers): WebasystClient
    {
        $this->default_wanet_headers = $default_wanet_headers;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->user_agent;
    }

    /**
     * @param string|null $user_agent
     * @return $this
     */
    public function setUserAgent(?string $user_agent): WebasystClient
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     */
    protected function _log($level, $message, $context = [])
    {
        if (!$this->logger || !$level) {
            return;
        }

        $this->logger->log($level, $message, $context);
    }

    /**
     * @return null|string
     */
    public function getLogLevel(): ?string
    {
        return $this->_log_level;
    }

    /**
     * @param null|string $log_level
     * @return $this
     */
    public function setLogLevel($log_level = null): WebasystClient
    {
        $this->_log_level = $log_level;
        return $this;
    }

    /**
     * @param \Exception $e
     */
    protected function _logException(\Exception $e)
    {
        $this->_log($this->getLogLevel(), "Got Exception. Code={code}, Message='{message}'", ['code' => $e->getCode(), 'message' => $e->getMessage()]);
    }
}
