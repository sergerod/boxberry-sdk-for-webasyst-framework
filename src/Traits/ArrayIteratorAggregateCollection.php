<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Traits;

/**
 * Trait ArrayIteratorAggregateCollection
 * @package SergeR\BoxberrySDK\Traits
 *
 * @property array $_items Must be declared
 */
trait ArrayIteratorAggregateCollection
{
    /**
     * Retrieve an external iterator
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->_items);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->_items);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->_items);
    }
}
