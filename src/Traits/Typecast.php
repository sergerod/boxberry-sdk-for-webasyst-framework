<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Traits;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use InvalidArgumentException;

/**
 * Trait Boolval
 * @package SergeR\BoxberrySDK\Traits
 */
trait Typecast
{
    /**
     * @param $value
     * @return bool
     */
    protected function _boolval($value)
    {
        if (!is_scalar($value)) {
            throw new InvalidArgumentException('Scalar value required to convert to boolean');
        }

        if (is_string($value)) {
            $value = mb_strtolower($value, 'UTF-8');
            $value = in_array($value, ['1', 'да', 'yes', 'ok', 'есть']) ? 1 : 0;
        }

        return (bool)$value;
    }

    /**
     * @param $value
     * @param bool $null
     * @return float|null
     */
    protected function _floatval($value, $null = false)
    {
        if ($value === $null) {
            return $null ? null : 0.0;
        }
        if (!is_scalar($value)) {
            throw new InvalidArgumentException('Scalar value required to convert to float');
        } elseif (is_float($value) || is_int($value)) {
            $value = (float)$value;
        } elseif (is_string($value)) {
            $value = (float)trim(str_replace(',', '.', $value));
        }
        return (float)$value;
    }

    /**
     * @param null|string|DateTimeInterface $date
     * @param bool $null
     * @return DateTimeInterface|null
     * @throws Exception
     */
    protected function _date($date, $null = false)
    {
        if ($date === null) {
            return $null ? null : new DateTimeImmutable();
        }
        if (is_object($date)) {
            if ($date instanceof DateTimeInterface) {
                return $date;
            }
            throw new InvalidArgumentException('Parameter must be a string representation of date/datetime or DateTimeInterface object');
        }
        if (is_string($date)) {
            $Date = trim($date);
            if (preg_match('/^\d{4}\.\d{2}\.\d{2}\s+\d{2}:\d{2}:\d{2}$/', $date) === 1) {
                return DateTimeImmutable::createFromFormat('Y.m.d H:i:s', $date);
            } elseif (preg_match('/^\d{4}\.\d{2}\.\d{2}$/', $Date) === 1) {
                return DateTimeImmutable::createFromFormat('Y.m.d', $date);
            } else {
                return new DateTimeImmutable($date);
            }
        }

        throw new InvalidArgumentException('Parameter must be a string representation of date/datetime or DateTimeInterface object');
    }
}