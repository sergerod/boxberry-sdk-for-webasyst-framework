<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\OrdersBalanceResponse;

/**
 * Class OrdersBalanceRequest
 * @package SergeR\BoxberrySDK\Request
 */
class OrdersBalanceRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'OrdersBalance'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new OrdersBalanceResponse($body);
    }
}