<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\CourierListCitiesResponse;

/**
 * Class CourierListCitiesRequest
 * @package SergeR\BoxberrySDK\Request
 */
class CourierListCitiesRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'CourierListCities'];
    }

    /**
     * @param array $headers
     * @param null $body
     * @return CourierListCitiesResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new CourierListCitiesResponse($body);
    }
}