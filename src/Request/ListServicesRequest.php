<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ListServicesResponse;

/**
 * Class ListServicesRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ListServicesRequest extends ListStatusesRequest
{
    public function getData()
    {
        return ['method' => 'ListServices'] + parent::getData();
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListServicesResponse($body);
    }
}