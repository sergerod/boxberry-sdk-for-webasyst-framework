<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\PointsDescriptionResponse;
use SergeR\CakeUtility\Hash;

/**
 * Class PointsDescriptionRequest
 * @package SergeR\BoxberrySDK\Request
 */
class PointsDescriptionRequest extends AbstractRequest
{
    /** @var string */
    protected $Code = '';

    /** @var bool */
    protected $Photo = false;

    public function __construct(array $params = [])
    {
        $this->Code = (string)Hash::get($params, 'Code');
        $this->Photo = (bool)Hash::get($params, 'Photo');
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @return bool
     */
    public function needPhoto()
    {
        return $this->Photo;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array_filter(['method' => 'PointsDescription', 'code' => $this->getCode(), 'photo' => (int)$this->needPhoto()]);
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new PointsDescriptionResponse($body);
    }
}