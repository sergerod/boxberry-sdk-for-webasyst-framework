<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListCitiesResponse;
use SergeR\BoxberrySDK\WebasystClient;

/**
 * Class ListCitiesFullRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ListCitiesResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ListCitiesFullRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return ['method' => 'ListCitiesFull'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ListCitiesResponse
     */
    protected function _deserializeResponse(array $headers, $body = null): ListCitiesResponse
    {
        $body = (array)$body;
        return new ListCitiesResponse($body);
    }
}
