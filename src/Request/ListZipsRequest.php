<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListZipsResponse;

/**
 * Class ListZipsRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ListZipsRequest extends AbstractRequest
{
    protected $_api_method = 'ListZips';

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ListZips'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ListZipsResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListZipsResponse($body);
    }
}