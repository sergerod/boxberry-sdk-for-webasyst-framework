<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ParselCheckResponse;
use SergeR\CakeUtility\Hash;

/**
 * Class ParselCheckRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ParselCheckRequest extends AbstractRequest
{
    protected $ImId = '';

    public function __construct(array $param = [])
    {
        $this->setImId(Hash::get($param, 'ImId'));
    }

    /**
     * @return string
     */
    public function getImId()
    {
        return $this->ImId;
    }

    /**
     * @param string $ImId
     * @return ParselCheckRequest
     */
    public function setImId($ImId)
    {
        $this->ImId = (string)$ImId;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ParselCheck', 'ImId' => $this->getImId()];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ParselCheckResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselCheckResponse($body);
    }
}