<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListPointsResponse;
use SergeR\BoxberrySDK\WebasystClient;
use SergeR\CakeUtility\Hash;

/**
 * Class ListPointsRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ListPointsResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ListPointsRequest extends AbstractRequest
{
    /** @var null|string */
    protected $CityCode = null;

    /** @var bool */
    protected $Prepaid = false;

    /**
     * ListPointsRequest constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->setCityCode(Hash::get($params, 'CityCode'));
        $this->setPrepaid((bool)Hash::get($params, 'Prepaid'));
    }

    /**
     * @return string|null
     */
    public function getCityCode(): ?string
    {
        return $this->CityCode;
    }

    /**
     * @param string|null $CityCode
     * @return ListPointsRequest
     */
    public function setCityCode(?string $CityCode = null): ListPointsRequest
    {
        $this->CityCode = $CityCode;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPrepaid(): bool
    {
        return $this->Prepaid;
    }

    /**
     * @param bool|int $Prepaid
     * @return ListPointsRequest
     */
    public function setPrepaid($Prepaid): ListPointsRequest
    {
        $this->Prepaid = (bool)$Prepaid;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return array_filter(['method' => 'ListPoints', 'CityCode' => $this->getCityCode(), 'prepaid' => (int)$this->isPrepaid()]);
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ListPointsResponse
     */
    protected function _deserializeResponse(array $headers, $body = null): ListPointsResponse
    {
        $body = (array)$body;
        return new ListPointsResponse($body);
    }
}
