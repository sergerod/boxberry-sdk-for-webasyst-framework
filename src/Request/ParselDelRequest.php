<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ParselDelResponse;
use SergeR\BoxberrySDK\WebasystClient;
use SergeR\CakeUtility\Hash;

/**
 * Class ParselDelRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ParselDelResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ParselDelRequest extends AbstractRequest
{
    protected $_api_method = 'ParselDel';

    /**
     * @var string
     */
    protected $ImId = '';

    /**
     * ParselDelRequest constructor.
     * @param array $param
     */
    public function __construct(array $param = [])
    {
        $this->setImId((string)Hash::get($param, 'ImId'));
    }

    /**
     * @return string
     */
    public function getImId()
    {
        return $this->ImId;
    }

    /**
     * @param string $ImId
     * @return ParselDelRequest
     */
    public function setImId($ImId)
    {
        $this->ImId = (string)$ImId;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ParselDel', 'ImId' => $this->getImId()];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselDelResponse($body);
    }
}