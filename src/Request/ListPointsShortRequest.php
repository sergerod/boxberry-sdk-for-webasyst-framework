<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListPointsShortResponse;
use SergeR\CakeUtility\Hash;

/**
 * Class ListPointsShortRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ListPointsShortRequest extends AbstractRequest
{
    /** @var null|string */
    protected $CityCode;

    /**
     * ListPointsShortRequest constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->setCityCode(Hash::get($params, 'CityCode'));
    }

    /**
     * @return string|null
     */
    public function getCityCode()
    {
        return $this->CityCode;
    }

    /**
     * @param string|null $CityCode
     * @return ListPointsShortRequest
     */
    public function setCityCode($CityCode)
    {
        $this->CityCode = $CityCode ? (string)$CityCode : null;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array_filter(['method' => 'ListPointsShort', 'CityCode' => $this->getCityCode()]);
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ListPointsShortResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListPointsShortResponse($body);
    }
}