<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListStatusesFullResponse;

/**
 * Class ListStatusesFullRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ListStatusesFullRequest extends ListStatusesRequest
{
    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ListStatusesFull'] + parent::getData();
    }

    /**
     * @param array $headers
     * @param null $body
     * @return ListStatusesFullResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListStatusesFullResponse($body);
    }
}