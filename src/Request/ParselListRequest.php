<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ParselListResponse;
use SergeR\BoxberrySDK\WebasystClient;

/**
 * Class ParselListRequest
 * @package SergeR\BoxberrySDK\Request
 *
 * @method ParselListResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ParselListRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ParselList'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ParselListResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselListResponse($body);
    }
}