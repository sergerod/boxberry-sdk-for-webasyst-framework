<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use Exception;
use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\CreateIntakeResponse;
use SergeR\BoxberrySDK\Traits\Typecast;
use SergeR\CakeUtility\Inflector;

/**
 * Class CreateIntakeRequest
 * @package SergeR\BoxberrySDK\Request
 */
class CreateIntakeRequest extends AbstractRequest
{
    use Typecast;

    /** @var string */
    protected $Zip = '';

    /** @var string */
    protected $City = '';

    /** @var string */
    protected $Street = '';

    /** @var string */
    protected $House = '';

    /** @var string */
    protected $Building = '';

    /** @var string */
    protected $Housing = '';

    /** @var string */
    protected $Flat = '';

    /** @var string */
    protected $ContactPerson = '';

    /** @var string */
    protected $ContactPhone = '';

    /** @var \DateTimeInterface */
    protected $TakingDate;

    /** @var string */
    protected $TakingTimeFrom = '10:00';

    /** @var string */
    protected $TakingTimeTo = '18:00';

    /** @var int */
    protected $SeatsCount = 0;

    /** @var float */
    protected $Volume = 0.0;

    /** @var float */
    protected $Weight = 0.0;

    /** @var string */
    protected $Comment = '';

    /**
     * CreateIntakeRequest constructor.
     * @param array $params
     * @throws Exception
     */
    public function __construct(array $params = [])
    {
        $this->TakingDate = new \DateTimeImmutable();

        foreach ($params as $key => $value) {
            if (substr($key, 0, 1) === '_') {
                continue;
            }
            $camelized = Inflector::camelize($key);
            $method = 'set' . $camelized;
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } elseif (property_exists($this, $key) || property_exists($this, $camelized)) {
                $prop = property_exists($this, $key) ? $key : $camelized;
                if (is_scalar($this->{$prop})) {
                    $type = gettype($this->{$prop});
                    if (settype($value, $type)) {
                        $this->{$prop} = $value;
                    }
                } elseif (is_object($this->{$prop})) {
                    $class = get_class($this->{$prop});
                    if (method_exists($class, 'fromArray')) {
                        $this->{$prop} = $class::fromArray((array($value)));
                    }
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->Zip;
    }

    /**
     * @param string $Zip
     * @return CreateIntakeRequest
     */
    public function setZip($Zip)
    {
        $this->Zip = (string)$Zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     * @return CreateIntakeRequest
     */
    public function setCity($City)
    {
        $this->City = (string)$City;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->Street;
    }

    /**
     * @param string $Street
     * @return CreateIntakeRequest
     */
    public function setStreet($Street)
    {
        $this->Street = (string)$Street;
        return $this;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->House;
    }

    /**
     * @param string $House
     * @return CreateIntakeRequest
     */
    public function setHouse($House)
    {
        $this->House = (string)$House;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->Building;
    }

    /**
     * @param string $Building
     * @return CreateIntakeRequest
     */
    public function setBuilding($Building)
    {
        $this->Building = (string)$Building;
        return $this;
    }

    /**
     * @return string
     */
    public function getHousing()
    {
        return $this->Housing;
    }

    /**
     * @param string $Housing
     * @return CreateIntakeRequest
     */
    public function setHousing($Housing)
    {
        $this->Housing = (string)$Housing;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlat()
    {
        return $this->Flat;
    }

    /**
     * @param string $Flat
     * @return CreateIntakeRequest
     */
    public function setFlat($Flat)
    {
        $this->Flat = (string)$Flat;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->ContactPerson;
    }

    /**
     * @param string $ContactPerson
     * @return CreateIntakeRequest
     */
    public function setContactPerson($ContactPerson)
    {
        $this->ContactPerson = (string)(string)$ContactPerson;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->ContactPhone;
    }

    /**
     * @param string $ContactPhone
     * @return CreateIntakeRequest
     */
    public function setContactPhone($ContactPhone)
    {
        $this->ContactPhone = (string)$ContactPhone;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTakingDate()
    {
        return clone $this->TakingDate;
    }

    /**
     * @param \DateTimeInterface $TakingDate
     * @return CreateIntakeRequest
     * @throws Exception
     */
    public function setTakingDate($TakingDate)
    {
        $this->TakingDate = $this->_date($TakingDate);
        return $this;
    }

    /**
     * @return string
     */
    public function getTakingTimeFrom()
    {
        return $this->TakingTimeFrom;
    }

    /**
     * @param string $TakingTimeFrom
     * @return CreateIntakeRequest
     */
    public function setTakingTimeFrom($TakingTimeFrom)
    {
        $this->TakingTimeFrom = (string)$TakingTimeFrom;
        return $this;
    }

    /**
     * @return string
     */
    public function getTakingTimeTo()
    {
        return $this->TakingTimeTo;
    }

    /**
     * @param string $TakingTimeTo
     * @return CreateIntakeRequest
     */
    public function setTakingTimeTo($TakingTimeTo)
    {
        $this->TakingTimeTo = (string)$TakingTimeTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeatsCount()
    {
        return $this->SeatsCount;
    }

    /**
     * @param int $SeatsCount
     * @return CreateIntakeRequest
     */
    public function setSeatsCount($SeatsCount)
    {
        $this->SeatsCount = (int)$SeatsCount;
        return $this;
    }

    /**
     * @return float
     */
    public function getVolume()
    {
        return $this->Volume;
    }

    /**
     * @param float $Volume
     * @return CreateIntakeRequest
     */
    public function setVolume($Volume)
    {
        $this->Volume = $this->_floatval($Volume);
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->Weight;
    }

    /**
     * @param float $Weight
     * @return CreateIntakeRequest
     */
    public function setWeight($Weight)
    {
        $this->Weight = $this->_floatval($Weight);
        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->Comment;
    }

    /**
     * @param string $Comment
     * @return CreateIntakeRequest
     */
    public function setComment($Comment)
    {
        $this->Comment = (string)$Comment;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array_filter(array(
            'method'           => 'CreateIntake',
            'zip'              => $this->getZip(),
            'city'             => $this->getCity(),
            'street'           => $this->getStreet(),
            'house'            => $this->getHouse(),
            'building'         => $this->getBuilding(),
            'housing'          => $this->getHousing(),
            'flat'             => $this->getFlat(),
            'contact_person'   => $this->getContactPerson(),
            'contact_phone'    => $this->getContactPhone(),
            'taking_date'      => $this->getTakingDate()->format('j.m.Y'),
            'taking_time_from' => $this->getTakingTimeFrom(),
            'taking_time_to'   => $this->getTakingTimeTo(),
            'seats_count'      => $this->getSeatsCount(),
            'volume'           => number_format($this->getVolume(), 3, '.', ''),
            'weight'           => number_format($this->getWeight(), 3, '.', ''),
            'comment'          => $this->getComment()
        ));
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return CreateIntakeResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new CreateIntakeResponse($body);
    }
}