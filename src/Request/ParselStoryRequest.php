<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ParselStoryResponse;
use SergeR\CakeUtility\Hash;

/**
 * Class ParselStoryRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ParselStoryRequest extends AbstractRequest
{
    /** @var DateTimeInterface|null */
    protected $From;

    /** @var DateTimeInterface|null */
    protected $To;

    /**
     * ParselStoryRequest constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $from = Hash::get($params, 'From');
        $to = Hash::get($params, 'To');

        try {
            if ($from) {
                $this->setFrom($from);
            }
        } catch (Exception $e) {
            $this->From = null;
        }
        if ($to) {
            try {
                $this->setTo($to);
            } catch (Exception $e) {
                $this->To = null;
            }
        }
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getFrom()
    {
        return $this->From;
    }

    /**
     * @param DateTimeInterface|string|null $From
     * @return ParselStoryRequest
     * @throws Exception
     */
    public function setFrom($From)
    {
        if (is_string($From)) {
            $From = new DateTimeImmutable($From);
        }

        $this->From = $From === null ? null : clone $From;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getTo()
    {
        return $this->To;
    }

    /**
     * @param DateTimeInterface|string|null $To
     * @return ParselStoryRequest
     * @throws Exception
     */
    public function setTo($To)
    {
        if (is_string($To)) {
            $To = new DateTimeImmutable($To);
        }

        $this->To = $To === null ? null : clone $To;

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array_filter(array(
            'method' => 'ParselStory',
            'from'   => $this->getFrom() ? $this->getFrom()->format('Ymd') : null,
            'to'     => $this->getTo() ? $this->getTo()->format('Ymd') : null
        ));
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselStoryResponse($body);
    }
}