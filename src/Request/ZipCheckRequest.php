<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use InvalidArgumentException;
use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ZipCheckResponse;
use SergeR\CakeUtility\Hash;

/**
 * Class ZipCheckRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ZipCheckRequest extends AbstractRequest
{
    /**
     * @var string
     */
    protected $Zip = '';

    /**
     * ZipCheckRequest constructor.
     * @param array $param
     */
    public function __construct(array $param)
    {
        $this->setZip(Hash::get($param, 'Zip'));
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->Zip;
    }

    /**
     * @param string $Zip
     * @return ZipCheckRequest
     */
    public function setZip($Zip)
    {
        if (!$Zip || !is_scalar($Zip)) {
            throw new InvalidArgumentException('Invalid Zip');
        }

        $this->Zip = $Zip;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ZipCheck', 'Zip' => $this->getZip()];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ZipCheckResponse($body);
    }
}