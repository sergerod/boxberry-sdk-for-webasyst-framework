<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ParselCreateResponse;
use SergeR\BoxberrySDK\Type\ParcelCourierDelivery;
use SergeR\BoxberrySDK\Type\ParcelCustomer;
use SergeR\BoxberrySDK\Type\ParcelDelivery;
use SergeR\BoxberrySDK\Type\ParcelItem;
use SergeR\BoxberrySDK\Type\ParcelItemCollection;
use SergeR\BoxberrySDK\Type\ParcelPoint;
use SergeR\BoxberrySDK\Type\ParcelPostalDelivery;
use SergeR\BoxberrySDK\Type\ParcelWeights;
use SergeR\BoxberrySDK\WebasystClient;
use SergeR\CakeUtility\Inflector;

/**
 * Class ParselCreateRequest
 * @package SergeR\BoxberrySDK\Request
 *
 * @method ParselCreateResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ParselCreateRequest extends AbstractRequest
{
    const DELIVERY_TYPE_PICKUP = 1;
    const DELIVERY_TYPE_COURIER = 2;
    const DELIVERY_TYPE_RUSSIANPOST = 3;

    const ISSUE_WITHOUT_OPEN = 0;
    const ISSUE_WITH_OPEN = 1;
    const ISSUE_PARTIAL = 2;

    protected $_api_method = 'ParselCreate';
    protected $UpdateByTrack;
    protected $OrderId = '';
    protected $PalletNumber;
    protected $Barcode;
    protected $Price = 0.0;
    protected $PaymentSum = 0.0;
    protected $DeliverySum = 0.0;
    protected $Vid = self::DELIVERY_TYPE_PICKUP;

    /** @var ParcelPoint */
    protected $Shop;
    /** @var ParcelCustomer */
    protected $Customer;
    /** @var ParcelDelivery */
    protected $Kurdost;

    /** @var ParcelItemCollection|null */
    protected $Items;

    /** @var ParcelWeights */
    protected $Weights;

    /** @var int|null */
    protected $issue;

    /** @var string|null */
    protected $sender_name;

    /**
     * ParselCreateRequest constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->Shop = new ParcelPoint();
        $this->Customer = new ParcelCustomer();
        $this->Weights = new ParcelWeights();
    }

    /**
     * @return string
     */
    public function getHttpMethod(): string
    {
        return 'POST';
    }

    /**
     * @param ParcelItem $item
     * @return $this
     */
    public function addItem(ParcelItem $item): ParselCreateRequest
    {
        if (!$this->Items) $this->Items = new ParcelItemCollection($item);
        else $this->Items->addItem($item);
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'method' => 'ParselCreate',
            'sdata'  => json_encode($this->_getData(), JSON_UNESCAPED_UNICODE)
        ];
    }

    /**
     * @return array
     */
    protected function _getData(): array
    {
        $order = [
            'updateByTrack' => $this->getUpdateByTrack(),
            'order_id'      => $this->getOrderId(),
            'PalletNumber'  => $this->getPalletNumber(),
            'barcode'       => $this->getBarcode(),
            'price'         => round($this->getPrice(), 2),
            'payment_sum'   => round($this->getPaymentSum(), 2),
            'delivery_sum'  => round($this->getDeliverySum(), 2),
            'vid'           => $this->getVid(),
            'shop'          => $this->getShop(),
            'customer'      => $this->getCustomer(),
            'items'         => $this->getItems(),
            'kurdost'       => $this->getKurdost(),
            'weights'       => $this->getWeights(),
            'issue'         => $this->getIssue(),
            'sender_name'   => $this->getSenderName()
        ];

        $order = array_filter($order, function ($v) {
            return $v !== null;
        });

        return $order;
    }

    /**
     * @return string
     */
    public function getUpdateByTrack(): ?string
    {
        return $this->UpdateByTrack;
    }

    /**
     * @param string|null $UpdateByTrack
     * @return ParselCreateRequest
     */
    public function setUpdateByTrack(?string $UpdateByTrack): ParselCreateRequest
    {
        $this->UpdateByTrack = $UpdateByTrack;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->OrderId;
    }

    /**
     * @param string $OrderId
     * @return ParselCreateRequest
     */
    public function setOrderId(string $OrderId): ParselCreateRequest
    {
        $this->OrderId = $OrderId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPalletNumber(): ?string
    {
        return $this->PalletNumber;
    }

    /**
     * @param string|null $PalletNumber
     * @return ParselCreateRequest
     */
    public function setPalletNumber(?string $PalletNumber = null): ParselCreateRequest
    {
        $this->PalletNumber = $PalletNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBarcode(): ?string
    {
        return $this->Barcode;
    }

    /**
     * @param string|null $Barcode
     * @return ParselCreateRequest
     */
    public function setBarcode(?string $Barcode): ParselCreateRequest
    {
        $this->Barcode = $Barcode;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->Price;
    }

    /**
     * @param float $Price
     * @return ParselCreateRequest
     */
    public function setPrice(float $Price): ParselCreateRequest
    {
        $this->Price = $Price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPaymentSum(): float
    {
        return $this->PaymentSum;
    }

    /**
     * @param float $PaymentSum
     * @return ParselCreateRequest
     */
    public function setPaymentSum(float $PaymentSum): ParselCreateRequest
    {
        $this->PaymentSum = $PaymentSum;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliverySum(): float
    {
        return $this->DeliverySum;
    }

    /**
     * @param float $DeliverySum
     * @return ParselCreateRequest
     */
    public function setDeliverySum(float $DeliverySum): ParselCreateRequest
    {
        $this->DeliverySum = $DeliverySum;
        return $this;
    }

    /**
     * @return int
     */
    public function getVid(): int
    {
        return $this->Vid;
    }

    /**
     * @param int $Vid
     * @return ParselCreateRequest
     */
    public function setVid(int $Vid): ParselCreateRequest
    {
        $this->Vid = $Vid;
        return $this;
    }

    /**
     * @return ParcelPoint
     */
    public function getShop(): ParcelPoint
    {
        return $this->Shop;
    }

    /**
     * @param ParcelPoint|array $Shop
     * @return ParselCreateRequest
     */
    public function setShop(ParcelPoint $Shop): ParselCreateRequest
    {
        $this->Shop = $Shop;
        return $this;
    }

    /**
     * @return ParcelCustomer
     */
    public function getCustomer(): ParcelCustomer
    {
        return $this->Customer;
    }

    /**
     * @param ParcelCustomer $Customer
     * @return ParselCreateRequest
     */
    public function setCustomer(ParcelCustomer $Customer): ParselCreateRequest
    {
        $this->Customer = $Customer;
        return $this;
    }

    /**
     * @return ParcelItemCollection|null
     */
    public function getItems(): ?ParcelItemCollection
    {
        return $this->Items;
    }

    /**
     * @param ParcelItemCollection|null $param
     * @return ParselCreateRequest
     */
    public function setItems(?ParcelItemCollection $param = null): ParselCreateRequest
    {
        $this->Items = $param;
        return $this;
    }

    /**
     * @return ParcelDelivery|ParcelCourierDelivery|ParcelPostalDelivery|null
     */
    public function getKurdost()
    {
        return $this->Kurdost;
    }

    /**
     * @param ParcelDelivery|null $param
     * @return ParselCreateRequest
     */
    public function setKurdost(?ParcelDelivery $param): ParselCreateRequest
    {
        $this->Kurdost = $param;
        return $this;
    }

    /**
     * @return ParcelWeights
     */
    public function getWeights(): ParcelWeights
    {
        return $this->Weights;
    }

    /**
     * @param ParcelWeights $param
     * @return ParselCreateRequest
     */
    public function setWeights($param): ParselCreateRequest
    {
        $this->Weights = $param;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIssue(): ?int
    {
        return $this->issue;
    }

    /**
     * @param int|null $issue
     * @return ParselCreateRequest
     */
    public function setIssue(?int $issue): ParselCreateRequest
    {
        if (!is_null($issue) && !in_array($issue, [self::ISSUE_WITHOUT_OPEN, self::ISSUE_WITH_OPEN, self::ISSUE_PARTIAL]))
            throw new \InvalidArgumentException('Parameter must be 0, 1, 2 or null');

        $this->issue = $issue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderName(): ?string
    {
        return $this->sender_name;
    }

    /**
     * @param string|null $sender_name
     * @return ParselCreateRequest
     */
    public function setSenderName(?string $sender_name): ParselCreateRequest
    {
        $this->sender_name = $sender_name;
        return $this;
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ParselCreateResponse
     */
    protected function _deserializeResponse(array $headers, $body = null): ParselCreateResponse
    {
        $body = (array)$body;
        return new ParselCreateResponse($body);
    }
}
