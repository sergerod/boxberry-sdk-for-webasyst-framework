<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\DeliveryCostsResponse;
use SergeR\BoxberrySDK\Traits\Typecast;

/**
 * Class DeliveryCostsRequest
 * @package SergeR\BoxberrySDK\Request
 */
class DeliveryCostsRequest extends AbstractRequest
{
    use Typecast;

    /** @var int Вес в граммах */
    protected $Weight = 0;

    /** @var string|null */
    protected $Target;

    /** @var float|null */
    protected $OrderSum;

    /** @var float|null */
    protected $DeliverySum;

    /** @var string|null */
    protected $TargetStart;

    /** @var int|null */
    protected $Height;

    /** @var int|null */
    protected $Width;

    /** @var int|null */
    protected $Depth;

    /** @var string|null */
    protected $Zip;

    /** @var float|null */
    protected $PaySum;

    /** @var bool */
    protected $Surcharge = false;

    public function __construct(array $params = [])
    {
        foreach ($params as $name => $param) {
            if (substr($name, 0, 1) === '_') {
                continue;
            }
            $method = 'set' . $name;
            if (method_exists($this, $method)) {
                $this->{$method}($param);
                continue;
            }
            if (property_exists($this, $name)) {
                $this->{$name} = $param;
            }
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return array_filter(array(
            'method'      => 'DeliveryCosts',
            'weight'      => $this->getWeight(),
            'target'      => $this->getTarget(),
            'ordersum'    => $this->getOrderSum() ? number_format($this->getOrderSum(), 2, '.', '') : null,
            'deliverysum' => $this->getDeliverySum() ? number_format($this->getDeliverySum(), 2, '.', '') : null,
            'targetstart' => $this->getTargetStart(),
            'height'      => (int)$this->getHeight(),
            'width'       => (int)$this->getWidth(),
            'depth'       => (int)$this->getDepth(),
            'zip'         => $this->getZip(),
            'paysum'      => $this->getPaySum() ? number_format($this->getPaySum(), 2, '.', '') : null,
            'surch'       => (int)$this->isSurcharge()
        ));
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->Weight;
    }

    /**
     * @param int $Weight
     * @return DeliveryCostsRequest
     */
    public function setWeight($Weight)
    {
        $this->Weight = (int)$Weight;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->Target;
    }

    /**
     * @param string|null $Target
     * @return DeliveryCostsRequest
     */
    public function setTarget($Target)
    {
        $this->Target = $Target ?: null;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderSum(): ?float
    {
        return $this->OrderSum;
    }

    /**
     * @param float|null $OrderSum
     * @return DeliveryCostsRequest
     */
    public function setOrderSum(?float $OrderSum): DeliveryCostsRequest
    {
        $this->OrderSum = $this->_floatval($OrderSum, true);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getDeliverySum(): ?float
    {
        return $this->DeliverySum;
    }

    /**
     * @param float|null $DeliverySum
     * @return DeliveryCostsRequest
     */
    public function setDeliverySum(?float $DeliverySum): DeliveryCostsRequest
    {
        $this->DeliverySum = $this->_floatval($DeliverySum, true);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetStart(): ?string
    {
        return $this->TargetStart;
    }

    /**
     * @param string|null $TargetStart
     * @return DeliveryCostsRequest
     */
    public function setTargetStart(?string $TargetStart): DeliveryCostsRequest
    {
        $this->TargetStart = $TargetStart ?: null;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->Height;
    }

    /**
     * @param int|null $Height
     * @return DeliveryCostsRequest
     */
    public function setHeight(?int $Height): DeliveryCostsRequest
    {
        $this->Height = $Height === null ? null : (int)$Height;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->Width;
    }

    /**
     * @param int|null $Width
     * @return DeliveryCostsRequest
     */
    public function setWidth(?int $Width): DeliveryCostsRequest
    {
        $this->Width = $Width === null ? null : (int)$Width;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDepth(): ?int
    {
        return $this->Depth;
    }

    /**
     * @param int|null $Depth
     * @return DeliveryCostsRequest
     */
    public function setDepth(?int $Depth): DeliveryCostsRequest
    {
        $this->Depth = $Depth === null ? null : (int)$Depth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->Zip;
    }

    /**
     * @param string|null $Zip
     * @return DeliveryCostsRequest
     */
    public function setZip(?string $Zip): DeliveryCostsRequest
    {
        $this->Zip = $Zip ?: null;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPaySum(): ?float
    {
        return $this->PaySum;
    }

    /**
     * @param float|null $PaySum
     * @return DeliveryCostsRequest
     */
    public function setPaySum(?float $PaySum): DeliveryCostsRequest
    {
        $this->PaySum = $this->_floatval($PaySum, true);
        return $this;
    }

    /**
     * @return bool
     */
    public function isSurcharge(): bool
    {
        return $this->Surcharge;
    }

    /**
     * @param bool $Surcharge
     * @return DeliveryCostsRequest
     */
    public function setSurcharge(bool $Surcharge): DeliveryCostsRequest
    {
        $this->Surcharge = (bool)$Surcharge;
        return $this;
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return DeliveryCostsResponse
     */
    protected function _deserializeResponse(array $headers, $body = null): DeliveryCostsResponse
    {
        $body = (array)$body;
        return new DeliveryCostsResponse($body);
    }
}
