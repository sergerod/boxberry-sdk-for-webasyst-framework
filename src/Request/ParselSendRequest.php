<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ParselSendResponse;
use SergeR\BoxberrySDK\WebasystClient;
use SergeR\CakeUtility\Hash;

/**
 * Class ParselSendRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ParselSendResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ParselSendRequest extends AbstractRequest
{
    protected $_api_method = 'ParselSend';

    /** @var string[] */
    protected $ImIds = [];

    /**
     * ParselSendRequest constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->setImIds($params);
    }

    /**
     * @return string[]
     */
    public function getImIds()
    {
        return $this->ImIds;
    }

    /**
     * @param string[] $ImIds
     * @return ParselSendRequest
     */
    public function setImIds(array $ImIds)
    {
        $this->ImIds = $ImIds;
        if (!empty($ImIds)) {
            if (Hash::numeric(array_keys($ImIds))) {
                $this->ImIds = $ImIds;
            } else {
                $this->ImIds = (array)Hash::get($ImIds, 'ImIds');
            }
        }
        return $this;
    }

    /**
     * @param string $id
     * @return ParselSendRequest
     */
    public function addInId($id)
    {
        $this->ImIds[] = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ParselSend', 'ImIds' => (string)$this];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(',', $this->getImIds());
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselSendResponse($body);
    }
}