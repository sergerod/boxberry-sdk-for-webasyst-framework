<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\PointsForParcelsResponse;
use SergeR\BoxberrySDK\WebasystClient;

/**
 * Class PointsForParcelsRequest
 * @package SergeR\BoxberrySDK\Request
 *
 * @method PointsForParcelsResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class PointsForParcelsRequest extends AbstractRequest
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return ['method' => 'PointsForParcels'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return PointsForParcelsResponse
     */
    protected function _deserializeResponse(array $headers, $body = null): PointsForParcelsResponse
    {
        $body = (array)$body;
        return new PointsForParcelsResponse($body);
    }
}
