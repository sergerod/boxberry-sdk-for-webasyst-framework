<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ParselSendStoryResponse;
use SergeR\BoxberrySDK\WebasystClient;

/**
 * Class ParselSendStoryRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ParselSendStoryResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ParselSendStoryRequest extends ParselStoryRequest
{
    protected $_api_method = 'ParselSendStory';

    public function getData()
    {
        return ['method' => 'ParselSendStory'] + parent::getData();
    }

    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ParselSendStoryResponse($body);
    }
}