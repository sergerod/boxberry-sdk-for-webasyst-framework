<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\ListCitiesResponse;

/**
 * Class ListCitiesRequest
 * @package SergeR\BoxberrySDK\Request
 */
class ListCitiesRequest extends AbstractRequest
{
    protected $_api_method = 'ListCities';

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ListCities'];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return ListCitiesResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListCitiesResponse($body);
    }
}