<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use InvalidArgumentException;
use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\Response\ListStatusesResponse;
use SergeR\BoxberrySDK\WebasystClient;
use SergeR\CakeUtility\Hash;

/**
 * Class ListStatusesRequest
 * @package SergeR\BoxberrySDK\Request
 * @method ListStatusesResponse waSend(WebasystClient $client, array $options = [], array $headers = [])
 */
class ListStatusesRequest extends AbstractRequest
{
    /** @var string */
    protected $ImId = '';

    /**
     * ListStatusesRequest constructor.
     * @param array $param
     */
    public function __construct(array $param = [])
    {
        $this->setImId((string)Hash::get($param, 'ImId'));
    }

    /**
     * @return string
     */
    public function getImId()
    {
        return $this->ImId;
    }

    /**
     * @param string $ImId
     * @return ListStatusesRequest
     */
    public function setImId(string $ImId): ListStatusesRequest
    {
        $this->ImId = (string)$ImId;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ['method' => 'ListStatuses', 'ImId' => $this->getImId()];
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    protected function _deserializeResponse(array $headers, $body = null)
    {
        $body = (array)$body;
        return new ListStatusesResponse($body);
    }
}
