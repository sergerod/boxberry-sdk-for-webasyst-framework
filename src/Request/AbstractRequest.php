<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Request;

use SergeR\BoxberrySDK\Response\AbstractResponse;
use SergeR\BoxberrySDK\WebasystClient;
use waException;

/**
 * Class AbstractRequest
 * @package SergeR\BoxberrySDK\Request
 */
abstract class AbstractRequest
{
    /**
     * @return array
     */
    abstract public function getData();

    protected function getHttpMethod()
    {
        return 'GET';
    }

    /**
     * @param array $headers
     * @param mixed $body
     * @return AbstractResponse
     */
    abstract protected function _deserializeResponse(array $headers, $body = null);

    /**
     * @param WebasystClient $client
     * @param array $options
     * @param array $headers
     * @return AbstractResponse
     * @throws waException
     */
    public function waSend(WebasystClient $client, array $options = [], array $headers = [])
    {
        $response = $client->query($this, $this->getHttpMethod(), $options, $headers);
        return $this->_deserializeResponse($response->getResponseHeader(), $response->getResponse());
    }
}