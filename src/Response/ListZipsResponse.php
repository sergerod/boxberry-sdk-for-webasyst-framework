<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListZipsCollection;

/**
 * Class ListZipsResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListZipsResponse extends AbstractResponse
{
    /** @var ListZipsCollection */
    protected $Zips;

    /**
     * ListZipsResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            $this->Zips = new ListZipsCollection();
            return;
        }

        $this->Zips = ListZipsCollection::fromArray($data);
    }

    /**
     * @return ListZipsCollection
     */
    public function getZips()
    {
        return clone $this->Zips;
    }

}