<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ParselStoryCollection;

/**
 * Class ParselStoryResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselStoryResponse extends AbstractResponse
{
    /** @var ParselStoryCollection */
    protected $Story;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if ($this->hasError()) {
            $this->Story = new ParselStoryCollection();
            return;
        }
        $this->Story = ParselStoryCollection::fromArray($data);
    }

    /**
     * @return ParselStoryCollection
     */
    public function getStory()
    {
        return clone $this->Story;
    }
}