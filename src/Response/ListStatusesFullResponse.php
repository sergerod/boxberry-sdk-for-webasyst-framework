<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Traits\Typecast;
use SergeR\BoxberrySDK\Type\ListStatusesCollection;
use SergeR\BoxberrySDK\Type\PartialDeliveryCollection;
use SergeR\CakeUtility\Hash;

/**
 * Class ListStatusesFullResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListStatusesFullResponse extends AbstractResponse
{
    use Typecast;

    /** @var ListStatusesCollection */
    protected $Statuses;

    /** @var bool */
    protected $PartialDelivery = false;

    /** @var null|float */
    protected $Sum;

    /** @var null|string */
    protected $PaymentMethod;

    /** @var null|float */
    protected $Weight;

    /** @var PartialDeliveryCollection */
    protected $Products;

    /**
     * ListStatusesFullResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->Products = PartialDeliveryCollection::fromArray((array)Hash::get($data, 'products'));
        $this->Statuses = ListStatusesCollection::fromArray((array)Hash::get($data, 'statuses'));
        if ($this->hasError()) {
            return;
        }

        $this->PartialDelivery = (bool)Hash::get($data, 'PD');
        $this->setSum(Hash::get($data, 'sum'));
        $this->setWeight(Hash::get($data, 'Weight'));
        $this->setPaymentMethod(Hash::get($data, 'PaymentMethod'));
    }

    /**
     * @param null|float $sum
     * @return $this
     */
    protected function setSum($sum)
    {
        if ($sum === null) {
            $this->Sum = $sum;
        } else {
            $this->Sum = $this->_floatval($sum);
        }

        return $this;
    }

    /**
     * @param float|null $weight
     * @return $this
     */
    protected function setWeight($weight)
    {
        if ($weight === null) {
            $this->Weight = $weight;
        } else {
            $this->Weight = $this->_floatval($weight);
        }

        return $this;
    }

    /**
     * @param string|null $method
     * @return $this
     */
    protected function setPaymentMethod($method)
    {
        if ($method === null) {
            $this->PaymentMethod = $method;
        } else {
            $this->PaymentMethod = (string)$method;
        }

        return $this;
    }

    /**
     * @return ListStatusesCollection
     */
    public function getStatuses()
    {
        return clone $this->Statuses;
    }

    /**
     * @return bool
     */
    public function isPartialDelivery()
    {
        return $this->PartialDelivery;
    }

    /**
     * @return float|null
     */
    public function getSum()
    {
        return $this->Sum;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->Weight;
    }

    /**
     * @return PartialDeliveryCollection
     */
    public function getProducts()
    {
        return clone $this->Products;
    }
}