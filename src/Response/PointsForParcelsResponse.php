<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\PointsForParcelsCollection;

/**
 * Class PointsForParcelsResponse
 * @package SergeR\BoxberrySDK\Response
 */
class PointsForParcelsResponse extends AbstractResponse
{
    /** @var PointsForParcelsCollection */
    protected $PointsForParcels;

    protected $raw = [];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if ($this->hasError()) {
            $this->PointsForParcels = new PointsForParcelsCollection();
            return;
        }

        $this->raw = $data;
        $this->PointsForParcels = PointsForParcelsCollection::fromArray($data);
    }

    /**
     * @return PointsForParcelsCollection
     */
    public function getPointsForParcels(): PointsForParcelsCollection
    {
        return clone $this->PointsForParcels;
    }

    /**
     * @return array
     */
    public function getRaw(): array
    {
        return $this->raw;
    }
}
