<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListStatusesCollection;
use SergeR\CakeUtility\Hash;

/**
 * Class ListStatusesResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListStatusesResponse extends AbstractResponse
{
    /** @var ListStatusesCollection */
    protected $Statuses;

    /**
     * ListStatusesResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if ($this->hasError()) {
            $this->Statuses = new ListStatusesCollection();
            return;
        }

        if (!empty($data) && (Hash::dimensions($data) < 2)) $data = [$data];

        $this->Statuses = ListStatusesCollection::fromArray($data);
    }

    /**
     * @return ListStatusesCollection
     */
    public function getStatuses(): ListStatusesCollection
    {
        return clone $this->Statuses;
    }
}
