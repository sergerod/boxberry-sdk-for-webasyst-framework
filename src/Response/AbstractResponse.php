<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class AbstractResponse
 * @package SergeR\BoxberrySDK\Response
 */
class AbstractResponse
{
    protected $_error;

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return (bool)$this->_error;
    }

    public function getError()
    {
        return $this->_error;
    }

    public function __construct(array $data = [])
    {
        if (count($data) <= 0) {
            $this->_error = 'No data received from Boxberry';
            return;
        }

        $err = Hash::get($data, '0.err');
        if ($err) {
            $this->_error = $err;
        }
    }
}
