<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class CreateIntakeResponse
 * @package SergeR\BoxberrySDK\Response
 */
class CreateIntakeResponse extends AbstractResponse
{
    protected $Message = '';

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (!$this->hasError()) {
            $this->Message = (string)Hash::get($data, 'message');
        }
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->Message;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->getMessage();
    }
}