<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use DateTimeInterface;
use Exception;
use SergeR\BoxberrySDK\Traits\Typecast;
use SergeR\BoxberrySDK\Type\ParselSendStoryCollection;
use SergeR\CakeUtility\Hash;

/**
 * Class ParselSendStoryResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselSendStoryResponse extends AbstractResponse
{
    use Typecast;

    /** @var ParselSendStoryCollection */
    protected $Story;

    /**
     * ParselSendStoryResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (($err = Hash::get($data, 'err'))) {
            $this->_error = $err;
        };

        if ($this->hasError()) {
            $this->Story = new ParselSendStoryCollection();
            return;
        }

        $this->Story = ParselSendStoryCollection::fromArray($data);
    }

    /**
     * @return ParselSendStoryCollection
     */
    public function getStory()
    {
        return clone $this->Story;
    }
}