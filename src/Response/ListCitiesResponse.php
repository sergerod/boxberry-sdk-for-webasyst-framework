<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListCitiesCollection;

/**
 * Class ListCitiesResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListCitiesResponse extends AbstractResponse
{
    /**
     * @var ListCitiesCollection
     */
    protected $Cities;

    /**
     * @var array
     */
    protected $raw = [];

    /**
     * ListCitiesResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            $this->Cities = new ListCitiesCollection();
            return;
        }

        $this->raw = $data;
        $this->Cities = ListCitiesCollection::fromArray($data);
    }

    /**
     * @return array
     */
    public function getRaw(): array
    {
        return $this->raw;
    }

    public function getCities(): ListCitiesCollection
    {
        return clone $this->Cities;
    }
}
