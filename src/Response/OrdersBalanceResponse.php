<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\OrdersBalanceCollection;
use SergeR\CakeUtility\Hash;

/**
 * Class OrdersBalanceResponse
 * @package SergeR\BoxberrySDK\Response
 */
class OrdersBalanceResponse extends AbstractResponse
{
    /** @var OrdersBalanceCollection */
    protected $Orders;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($err = $data['err'] ?: null)
            $this->_error = $err;

        if ($this->hasError()) {
            $this->Orders = new OrdersBalanceCollection();
            return;
        }

        $data = array_filter($data, function ($v) {
            return Hash::get($v, 'ID') !== null && Hash::get($v, 'Status') !== null && Hash::get($v, 'Price') !== null;
        });

        $this->Orders = OrdersBalanceCollection::fromArray($data);
    }

    /**
     * @return OrdersBalanceCollection
     */
    public function getOrders()
    {
        return clone $this->Orders;
    }
}
