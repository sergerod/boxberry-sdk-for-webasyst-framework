<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class ParselCreateResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselCreateResponse extends AbstractResponse
{
    protected $Track = '';
    protected $Label = '';

    public function __construct(array $data = [])
    {
        if (empty($data)) {
            parent::__construct($data);
        } elseif (($err = (string)Hash::get($data, 'err'))) {
            $this->_error = $err;
        }
        if ($this->hasError()) {
            return;
        }

        $this->Track = (string)Hash::get($data, 'track');
        $this->Label = (string)Hash::get($data, 'label');
    }

    /**
     * @return string
     */
    public function getTrack()
    {
        return $this->Track;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->Label;
    }
}