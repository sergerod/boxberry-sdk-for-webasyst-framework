<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListPointsCollection;

/**
 * Class ListPointsResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListPointsResponse extends AbstractResponse
{
    /** @var ListPointsCollection */
    protected $Points;

    /** @var array */
    protected $raw = [];

    /**
     * ListPointsResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            $this->Points = new ListPointsCollection();
            return;
        }

        $this->raw = $data;
        $this->Points = ListPointsCollection::fromArray($data);
    }

    /**
     * @return ListPointsCollection
     */
    public function getPoints(): ListPointsCollection
    {
        return $this->Points;
    }

    /**
     * @return array
     */
    public function getRaw(): array
    {
        return $this->raw;
    }
}
