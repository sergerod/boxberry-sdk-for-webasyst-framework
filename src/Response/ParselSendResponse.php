<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class ParselSendResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselSendResponse extends AbstractResponse
{
    protected $Id = '';
    protected $Label = '';
    protected $Sticker = '';

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if (($err = Hash::get($data, 'err'))) {
            $this->_error = $err;
        };

        if (!$this->hasError()) {
            $this->Id = Hash::get($data, 'id');
            $this->Label = Hash::get($data, 'label');
            $this->Sticker = Hash::get($data, 'sticker');
        }
    }

    /**
     * @return mixed|string
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @return mixed|string
     */
    public function getLabel()
    {
        return $this->Label;
    }

    /**
     * @return mixed|string
     */
    public function getSticker()
    {
        return $this->Sticker;
    }
}