<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019-2021
 * @license MIT
 */

declare(strict_types=1);

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class ZipCheckResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ZipCheckResponse extends AbstractResponse
{
    /** @var bool */
    protected $ExpressDelivery = false;

    /** @var string */
    protected $ZoneExpressDelivery = '';

    /**
     * ZipCheckResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if (!$this->hasError()) {
            $this->ExpressDelivery = (bool)Hash::get($data, '0.ExpressDelivery');
            $this->ZoneExpressDelivery = (string)Hash::get($data, '0.ZoneExpressDelivery');
        }
    }

    /**
     * @return bool
     */
    public function isExpressDelivery(): bool
    {
        return $this->ExpressDelivery;
    }

    /**
     * @return string
     */
    public function getZoneExpressDelivery(): string
    {
        return $this->ZoneExpressDelivery;
    }
}
