<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class ParselListResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselListResponse extends AbstractResponse
{
    /** @var string[] */
    protected $ImIds = [];

    /**
     * ParselListResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (($err = Hash::get($data, 'err'))) {
            $this->_error = $err;
        };
        if ($this->hasError()) {
            return;
        }

        $this->ImIds = explode(',', (string)Hash::get($data, 'ImIds'));
    }

    /**
     * @return string[]
     */
    public function getImIds()
    {
        return $this->ImIds;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(',', $this->getImIds());
    }
}