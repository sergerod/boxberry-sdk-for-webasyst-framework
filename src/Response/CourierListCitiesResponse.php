<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\CourierListCitiesCollection;

/**
 * Class CourierListCitiesResponse
 * @package SergeR\BoxberrySDK\Response
 */
class CourierListCitiesResponse extends AbstractResponse
{
    /** @var CourierListCitiesCollection */
    protected $Cities;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if ($this->hasError()) {
            $this->Cities = new CourierListCitiesCollection();
            return;
        }

        $this->Cities = CourierListCitiesCollection::fromArray($data);
    }

    /**
     * @return CourierListCitiesCollection
     */
    public function getCities()
    {
        return clone $this->Cities;
    }
}