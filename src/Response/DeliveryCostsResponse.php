<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Traits\Typecast;
use SergeR\CakeUtility\Hash;
use SergeR\CakeUtility\Inflector;

/**
 * Class DeliveryCostsResponse
 * @package SergeR\BoxberrySDK\Response
 */
class DeliveryCostsResponse extends AbstractResponse
{
    use Typecast;

    /** @var float */
    protected $Price = 0.0;

    /** @var float */
    protected $PriceBase = 0.0;

    /** @var float */
    protected $PriceService = 0.0;

    /** @var int|null */
    protected $DeliveryPeriod;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (!$this->hasError()) {
            foreach (['price', 'price_base', 'price_service'] as $item) {
                $val = $this->_floatval(Hash::get($data, $item));
                $item = Inflector::camelize($item);
                if (property_exists($this, $item)) {
                    $this->{$item} = $val;
                }
            }

            $dp = Hash::get($data, 'delivery_period');
            $this->DeliveryPeriod = $dp === null ? null : (int)$dp;
        }
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->Price;
    }

    /**
     * @return float
     */
    public function getPriceBase(): float
    {
        return $this->PriceBase;
    }

    /**
     * @return float
     */
    public function getPriceService(): float
    {
        return $this->PriceService;
    }

    /**
     * @return int|null
     */
    public function getDeliveryPeriod(): ?int
    {
        return $this->DeliveryPeriod;
    }
}
