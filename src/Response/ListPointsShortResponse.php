<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListPointsShortCollection;

/**
 * Class ListPointsShortResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListPointsShortResponse extends AbstractResponse
{
    /** @var ListPointsShortCollection */
    protected $Points;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            $this->Points = new ListPointsShortCollection();
            return;
        }

        $this->Points = ListPointsShortCollection::fromArray($data);
    }

    /**
     * @return ListPointsShortCollection
     */
    public function getPoints()
    {
        return clone $this->Points;
    }

}