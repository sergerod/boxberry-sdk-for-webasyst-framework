<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

/**
 * Class PointsDescriptionResponse
 * @package SergeR\BoxberrySDK\Response
 */
class PointsDescriptionResponse extends AbstractResponse
{
    protected $extraCode = '';
    protected $Name = '';
    protected $Organization = '';
    protected $ZipCode = '';
    protected $Country = '';
    protected $CountryCode = '';
    protected $Area = '';
    protected $CityCode = '';
    protected $CityName = '';
    protected $Settlement = '';
    protected $Metro = '';
    protected $Street = '';
    protected $House = '';
    protected $Structure = '';
    protected $Housing = '';
    protected $Apartment = '';
    protected $Address = '';
    protected $AddressReduce = '';
    protected $AddressInfo = '';
    protected $GPS = '';
    protected $TripDescription = '';
    protected $Phone = '';
    protected $ForeignOnlineStoresOnly = false;
    protected $PrepaidOrdersOnly = false;
    protected $Acquiring = false;
    protected $DigitalSignature = false;
    protected $TypeOfOffice = '';
    protected $CourierDelivery = false;
    protected $ReceptionLaP = false;
    protected $DeliveryLaP = false;
    protected $LoadLimit = 0;
    protected $VolumeLimit = 0.0;
    protected $EnablePartialDelivery = false;
    protected $EnableFitting = false;
    protected $WorkShedule = '';
    protected $WorkMoBegin = '';
    protected $WorkMoEnd = '';
    protected $WorkTuBegin = '';
    protected $WorkTuEnd = '';
    protected $WorkWeBegin = '';
    protected $WorkWeEnd = '';
    protected $WorkThBegin = '';
    protected $WorkThEnd = '';
    protected $WorkFrBegin = '';
    protected $WorkFrEnd = '';
    protected $WorkSaBegin = '';
    protected $WorkSaEnd = '';
    protected $WorkSuBegin = '';
    protected $WorkSuEnd = '';
    protected $LunchMoBegin = '';
    protected $LunchMoEnd = '';
    protected $LunchTuBegin = '';
    protected $LunchTuEnd = '';
    protected $LunchWeBegin = '';
    protected $LunchWeEnd = '';
    protected $LunchThBegin = '';
    protected $LunchThEnd = '';
    protected $LunchFrBegin = '';
    protected $LunchFrEnd = '';
    protected $LunchSaBegin = '';
    protected $LunchSaEnd = '';
    protected $LunchSuBegin = '';
    protected $LunchSuEnd = '';
    protected $TemporaryWorkSchedule = '';
    protected $Photos = [];
    protected $Terminal = false;
    protected $TerminalCode = '';
    protected $TerminalName = '';
    protected $TerminalOrganization = '';
    protected $TerminalCityCode = '';
    protected $TerminalCityName = '';
    protected $TerminalAddress = '';
    protected $TerminalPhone = '';
    protected $Reception = '';
    protected $ExpressReception = false;
    protected $TransType = 0;
    protected $InterRefunds = false;
    protected $IssuanceBoxberry = false;

    protected $_unknown_fields = [];

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            return;
        }

        foreach ($data as $key => $datum) {
            if (substr($key, 0, 1) === '_') {
                continue;
            }

            $method = 'set' . $key;
            if (method_exists($this, $method)) {
                $this->{$method}($datum);
            } elseif (property_exists($this, $key)) {
                if (is_scalar($this->{$key})) {
                    $type = gettype($this->{$key});
                    if (settype($datum, $type)) {
                        $this->{$key} = $datum;
                    }
                } elseif ($this->{$key} === null) {
                    $this->{$key} = $datum;
                } elseif (is_array($this->{$key})) {
                    $this->{$key} = (array)$datum;
                }
            } else {
                $this->_unknown_fields[$key] = $datum;
            }
        }
    }

    /**
     * @param array|null $photos
     * @return $this
     */
    protected function setPhotos(array $photos = null)
    {
        $photos = (array)$photos;
        $_decoded = [];
        foreach ($photos as $photo) {
            $photo = base64_decode($photo);
            if ($photo !== false) {
                $_decoded[] = $photo;
            }
        }

        $this->Photos = $_decoded;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtraCode()
    {
        return $this->extraCode;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->Organization;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->ZipCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->CountryCode;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->Area;
    }

    /**
     * @return string
     */
    public function getCityCode()
    {
        return $this->CityCode;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->CityName;
    }

    /**
     * @return string
     */
    public function getSettlement()
    {
        return $this->Settlement;
    }

    /**
     * @return string
     */
    public function getMetro()
    {
        return $this->Metro;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->Street;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->House;
    }

    /**
     * @return string
     */
    public function getStructure()
    {
        return $this->Structure;
    }

    /**
     * @return string
     */
    public function getHousing()
    {
        return $this->Housing;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->Apartment;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @return string
     */
    public function getAddressReduce()
    {
        return $this->AddressReduce;
    }

    /**
     * @return string
     */
    public function getAddressInfo()
    {
        return $this->AddressInfo;
    }

    /**
     * @return string
     */
    public function getGPS()
    {
        return $this->GPS;
    }

    /**
     * @return string
     */
    public function getTripDescription()
    {
        return $this->TripDescription;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @return bool
     */
    public function isForeignOnlineStoresOnly()
    {
        return $this->ForeignOnlineStoresOnly;
    }

    /**
     * @return bool
     */
    public function isPrepaidOrdersOnly()
    {
        return $this->PrepaidOrdersOnly;
    }

    /**
     * @return bool
     */
    public function isAcquiring()
    {
        return $this->Acquiring;
    }

    /**
     * @return bool
     */
    public function isDigitalSignature()
    {
        return $this->DigitalSignature;
    }

    /**
     * @return string
     */
    public function getTypeOfOffice()
    {
        return $this->TypeOfOffice;
    }

    /**
     * @return bool
     */
    public function isCourierDelivery()
    {
        return $this->CourierDelivery;
    }

    /**
     * @return bool
     */
    public function isReceptionLaP()
    {
        return $this->ReceptionLaP;
    }

    /**
     * @return bool
     */
    public function isDeliveryLaP()
    {
        return $this->DeliveryLaP;
    }

    /**
     * @return int
     */
    public function getLoadLimit()
    {
        return $this->LoadLimit;
    }

    /**
     * @return float
     */
    public function getVolumeLimit()
    {
        return $this->VolumeLimit;
    }

    /**
     * @return bool
     */
    public function isEnablePartialDelivery()
    {
        return $this->EnablePartialDelivery;
    }

    /**
     * @return bool
     */
    public function isEnableFitting()
    {
        return $this->EnableFitting;
    }

    /**
     * @return string
     */
    public function getWorkShedule()
    {
        return $this->WorkShedule;
    }

    /**
     * @return string
     */
    public function getWorkMoBegin()
    {
        return $this->WorkMoBegin;
    }

    /**
     * @return string
     */
    public function getWorkMoEnd()
    {
        return $this->WorkMoEnd;
    }

    /**
     * @return string
     */
    public function getWorkTuBegin()
    {
        return $this->WorkTuBegin;
    }

    /**
     * @return string
     */
    public function getWorkTuEnd()
    {
        return $this->WorkTuEnd;
    }

    /**
     * @return string
     */
    public function getWorkWeBegin()
    {
        return $this->WorkWeBegin;
    }

    /**
     * @return string
     */
    public function getWorkWeEnd()
    {
        return $this->WorkWeEnd;
    }

    /**
     * @return string
     */
    public function getWorkThBegin()
    {
        return $this->WorkThBegin;
    }

    /**
     * @return string
     */
    public function getWorkThEnd()
    {
        return $this->WorkThEnd;
    }

    /**
     * @return string
     */
    public function getWorkFrBegin()
    {
        return $this->WorkFrBegin;
    }

    /**
     * @return string
     */
    public function getWorkFrEnd()
    {
        return $this->WorkFrEnd;
    }

    /**
     * @return string
     */
    public function getWorkSaBegin()
    {
        return $this->WorkSaBegin;
    }

    /**
     * @return string
     */
    public function getWorkSaEnd()
    {
        return $this->WorkSaEnd;
    }

    /**
     * @return string
     */
    public function getWorkSuBegin()
    {
        return $this->WorkSuBegin;
    }

    /**
     * @return string
     */
    public function getWorkSuEnd()
    {
        return $this->WorkSuEnd;
    }

    /**
     * @return string
     */
    public function getLunchMoBegin()
    {
        return $this->LunchMoBegin;
    }

    /**
     * @return string
     */
    public function getLunchMoEnd()
    {
        return $this->LunchMoEnd;
    }

    /**
     * @return string
     */
    public function getLunchTuBegin()
    {
        return $this->LunchTuBegin;
    }

    /**
     * @return string
     */
    public function getLunchTuEnd()
    {
        return $this->LunchTuEnd;
    }

    /**
     * @return string
     */
    public function getLunchWeBegin()
    {
        return $this->LunchWeBegin;
    }

    /**
     * @return string
     */
    public function getLunchWeEnd()
    {
        return $this->LunchWeEnd;
    }

    /**
     * @return string
     */
    public function getLunchThBegin()
    {
        return $this->LunchThBegin;
    }

    /**
     * @return string
     */
    public function getLunchThEnd()
    {
        return $this->LunchThEnd;
    }

    /**
     * @return string
     */
    public function getLunchFrBegin()
    {
        return $this->LunchFrBegin;
    }

    /**
     * @return string
     */
    public function getLunchFrEnd()
    {
        return $this->LunchFrEnd;
    }

    /**
     * @return string
     */
    public function getLunchSaBegin()
    {
        return $this->LunchSaBegin;
    }

    /**
     * @return string
     */
    public function getLunchSaEnd()
    {
        return $this->LunchSaEnd;
    }

    /**
     * @return string
     */
    public function getLunchSuBegin()
    {
        return $this->LunchSuBegin;
    }

    /**
     * @return string
     */
    public function getLunchSuEnd()
    {
        return $this->LunchSuEnd;
    }

    /**
     * @return string
     */
    public function getTemporaryWorkSchedule()
    {
        return $this->TemporaryWorkSchedule;
    }

    /**
     * @return array
     */
    public function getPhotos()
    {
        return $this->Photos;
    }

    /**
     * @return bool
     */
    public function isTerminal()
    {
        return $this->Terminal;
    }

    /**
     * @return string
     */
    public function getTerminalCode()
    {
        return $this->TerminalCode;
    }

    /**
     * @return string
     */
    public function getTerminalName()
    {
        return $this->TerminalName;
    }

    /**
     * @return string
     */
    public function getTerminalOrganization()
    {
        return $this->TerminalOrganization;
    }

    /**
     * @return string
     */
    public function getTerminalCityCode()
    {
        return $this->TerminalCityCode;
    }

    /**
     * @return string
     */
    public function getTerminalCityName()
    {
        return $this->TerminalCityName;
    }

    /**
     * @return string
     */
    public function getTerminalAddress()
    {
        return $this->TerminalAddress;
    }

    /**
     * @return string
     */
    public function getTerminalPhone()
    {
        return $this->TerminalPhone;
    }

    /**
     * @return string
     */
    public function getReception()
    {
        return $this->Reception;
    }

    /**
     * @return bool
     */
    public function isExpressReception()
    {
        return $this->ExpressReception;
    }

    /**
     * @return int
     */
    public function getTransType()
    {
        return $this->TransType;
    }

    /**
     * @return bool
     */
    public function isInterRefunds()
    {
        return $this->InterRefunds;
    }

    /**
     * @return bool
     */
    public function isIssuanceBoxberry()
    {
        return $this->IssuanceBoxberry;
    }
}