<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\CakeUtility\Hash;

/**
 * Class ParselCheckResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselCheckResponse extends AbstractResponse
{
    /**
     * @var string
     */
    protected $Label = '';

    /**
     * ParselCheckResponse constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (!$this->hasError()) {
            $this->Label = (string)Hash::get($data, 'label');
        }
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->Label;
    }
}