<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

/**
 * Class ParselDelResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ParselDelResponse extends AbstractResponse
{

}