<?php
/**
 * @author Serge Rodovnichenko, <serge@syrnik.com>
 * @copyright Serge Rodovnichenko, 2019
 * @license MIT
 */

namespace SergeR\BoxberrySDK\Response;

use SergeR\BoxberrySDK\Type\ListServicesCollection;

/**
 * Class ListServicesResponse
 * @package SergeR\BoxberrySDK\Response
 */
class ListServicesResponse extends AbstractResponse
{
    /** @var ListServicesCollection */
    protected $Services;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        if ($this->hasError()) {
            $this->Services = new ListServicesCollection();
            return;
        }

        $this->Services = ListServicesCollection::fromArray($data);
    }

    /**
     * @return ListServicesCollection
     */
    public function getServices()
    {
        return clone $this->Services;
    }
}